<?php 

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
// use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * 
 */
class BaseKernelTestCase extends KernelTestCase
{
    public function setUp()
    {
        self::bootKernel();
    }
}
