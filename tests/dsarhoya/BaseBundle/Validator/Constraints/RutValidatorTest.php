<?php 

namespace Tests\dsarhoya\BaseBundle;

use Tests\BaseKernelTestCase;
use dsarhoya\BaseBundle\Validator\Constraints\RUT;

/**
 * 
 */
class RutValidatorTest extends BaseKernelTestCase
{
    /**
     * @dataProvider getRutTests
     */
    public function testItvalidatesRust($rut, $errorCount){
        $validator = self::$kernel->getContainer()->get('validator');
        $this->assertCount($errorCount, $validator->validate($rut, new RUT()));
    }
    
    public function getRutTests(){
        return [
            // rut, cantidad de errores
            ['1-9', 0],
            ['13.7.5.9.9,,,79-1', 0],
            ['23799728-k', 0],
            ['23799728-K', 0],
            ['23.799.728-k', 0],
            ['23,799,728-k', 0],
            ['esto nada que ver', 1],
            ['23799728-1', 1],
            ['19', 1],
            ['1 9', 1],
            [null, 1],
            [19, 1],
        ];
    }
}
