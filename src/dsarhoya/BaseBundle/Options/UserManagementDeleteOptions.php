<?php

namespace dsarhoya\BaseBundle\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of UserManagementDeleteOptions
 *
 * @author matias
 */
class UserManagementDeleteOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'force'     => false,
        ));
        
        $resolver->setAllowedTypes('force', array('null', 'bool'));
    }
}
