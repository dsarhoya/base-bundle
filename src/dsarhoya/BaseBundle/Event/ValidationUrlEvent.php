<?php 

namespace dsarhoya\BaseBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * 
 */
class ValidationUrlEvent extends Event
{
    public function __construct($user = null){
        $this->setUser($user);
    }
    /**
     * @var string
     */
    private $url;
    
    /**
     * @var mixed
     */
    private $user;

    /**
     * Get the value of Url 
     * 
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
 
    /** 
     * Set the value of Url 
     * 
     * @param string url
     * 
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;
 
        return $this;
    }
    
    /**
     * Get the value of User 
     * 
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
 
    /** 
     * Set the value of User 
     * 
     * @param mixed user
     * 
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;
 
        return $this;
    }
 
}
