<?php

namespace dsarhoya\BaseBundle\Event;

/**
 * Description of BaseBundleEvents
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
final class BaseBundleEvents {
    const LOGIN = 'base_bundle.login';
    //Event Validate User Account
    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;") */
    const POST_VALIDATE_ACCOUNT = 'base_bundle.post_validate_account';
    //Recuperacion de contraseña
    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;") */
    const POST_RETRIEVE_PASSWORD = 'base_bundle.post_retrieve_password';
    //Generación de url de validación
    /** @Event("dsarhoya\BaseBundle\Event\ValidationUrlEvent;") */
    const GENERATE_VALIDATION_URL = 'base_bundle.generate_validation_url';
}
