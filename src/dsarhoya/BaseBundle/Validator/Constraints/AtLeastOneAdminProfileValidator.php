<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AtLeastOneAdminProfileValidator extends ConstraintValidator
{
    private $em;
    private $user_class;
    private $profile_class;
    private $loggedUser;
    
    public function __construct($entity_manager, $securityContext, $classes) {
        $this->em = $entity_manager;
        $this->user_class = $classes['user']['class'];
        $this->profile_class = $classes['profile']['class'];
        $this->loggedUser = $securityContext->getToken()->getUser();
    }
    
    public function validate($user, Constraint $constraint){
        
        $adminUsers = $this->em->getRepository('dsarhoya\BaseBundle\Entity\BaseUser')
                ->adminUsers($user->getCompany(), $this->user_class);
        
        if(is_null($user->getId())){
            $this->validateNew($user, $adminUsers);
        }else{
            $this->validateExisting($user, $adminUsers);
        }
        
    }
    
    public function validateNew($user, $adminUsers) {
        
        if(count($adminUsers) > 0) return;
        
        if(is_null($user->getProfile())||(!$user->getProfile()->getIsAdmin())){
            $this->context->addViolation('Tiene que haber al menos un usuario con perfil administrador.');
        }
    }
    
    public function validateExisting($user, $adminUsers) {
        
        if(count($adminUsers) > 1){
            return;
        }
        
        if(count($adminUsers) === 1 &&(!$this->userInArray($user, $adminUsers))){
            return;
        }
        
        if(is_null($user->getProfile())||(!$user->getProfile()->getIsAdmin())){
            $this->context->addViolation('Tiene que haber al menos un usuario con perfil administrador.');
        }
    }
    
    private function userInArray($user, $adminUsers){
        foreach ($adminUsers as $adminUser) {
            if($adminUser->getId() === $user->getId()) return true;
        }
        return false;
    }
}