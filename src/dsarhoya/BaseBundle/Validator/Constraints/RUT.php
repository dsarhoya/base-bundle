<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class RUT extends Constraint
{
    public $message = 'Rut inválido';
    public $ignoreNull = false;
}