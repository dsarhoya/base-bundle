<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RUTValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        //Verifica si es nulo y si esta habilitado el ignoreNull
        if((is_null($value) || $value == '') && $constraint->ignoreNull) {return $this;}
        elseif((is_null($value) || $value == '') && !$constraint->ignoreNull){
            $this->context->buildViolation("Ingrese un RUT válido.")
                ->atPath('rut')
                ->addViolation();
            return $this;
        }
        
        //Valida el RUT
        if (!$this->valida_rut($value)) {
            $this->context->addViolation($constraint->message);
            return $this;
        }
    }
    
    private function valida_rut($rut_con_verificador){
        if(is_int($rut_con_verificador)) {
            $rut_con_verificador = (string) $rut_con_verificador;
        }

        $rut_con_verificador = str_replace(".", "", $rut_con_verificador);
        $rut_con_verificador = str_replace(",", "", $rut_con_verificador);
        $rut_con_verificador = str_replace(" ", "", $rut_con_verificador);

        if (count(explode("-", $rut_con_verificador)) != 2) {
            return false;
        }

        $auxRut = explode("-", $rut_con_verificador);
        $rut = $auxRut[0];
        $verificador = $auxRut[1];
        
        if(!is_numeric($rut)){
            return false;
        }
        
        if(!is_numeric($verificador) && strtolower((string)$verificador) !== 'k'){
            return false;
        }

        $x = 2;
        $sumatorio = 0;
        for ($i = strlen($rut) - 1; $i >= 0; $i--) {
            if ($x > 7) {
                $x = 2;
            }
            $sumatorio = $sumatorio + ($rut[$i] * $x);
            $x++;
        }
        $digito = bcmod($sumatorio, 11);
        $digito = 11 - $digito;
        switch ($digito) {
            case 10:
                $digito = "K";
                break;
            case 11:
                $digito = "0";
                break;
        }
        
        if (strtolower($verificador) == strtolower($digito)) {
            return true;
        } else {
            return false;
        }
    }
}
