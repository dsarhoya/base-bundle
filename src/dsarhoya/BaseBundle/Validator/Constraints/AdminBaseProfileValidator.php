<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use dsarhoya\BaseBundle\Entity\BaseProfile;

/**
 * Description of newPHPClass
 *
 * @author matias
 */
class AdminBaseProfileValidator extends ConstraintValidator
{
    private $em;
    private $profile_class;
    
    public function __construct($entity_manager, $classes) {
        $this->em = $entity_manager;
        $this->profile_class = $classes['profile']['class'];
    }
    
    public function validate($profile, Constraint $constraint)
    {
        if(is_null($profile->getCompany())){
            $this->context->addViolation('Un perfil tiene que estar asociado a una empresa.');
            return;
        }
        
        if($profile->getId()){
            return $this->validateExisting($profile);
        }else{
            return $this->validateNew($profile);
        }
    }
    
    private function validateNew(BaseProfile $profile){
        
        $repo = $this->em->getRepository($this->profile_class);
        $admin_profile = $repo->findOneBy(array(
            'company'=>$profile->getCompany(),
            'isAdmin'=>true
        ));
        
        if(is_null($admin_profile) && !$profile->getIsAdmin()){
            $this->context->addViolation('Debe haber al menos un perfil de administrador en la empresa');
            return;
        }
        
        if(!is_null($admin_profile) && $profile->getIsAdmin()){
            $this->context->addViolation('Ya hay un perfil administrador en esta empresa');
            return;
        }
    }

    private function validateExisting(BaseProfile $profile){
        
        $repo = $this->em->getRepository('dsarhoya\BaseBundle\Entity\BaseProfile');
        $admin_profile = $repo->otherAdminProfile($profile, $this->profile_class);
        
        if(is_null($admin_profile) && !$profile->getIsAdmin()){
            $this->context->addViolation('Debe haber al menos un perfil de administrador en la empresa');
            return;
        }
        
        if(!is_null($admin_profile) && $profile->getIsAdmin()){
            $this->context->addViolation('Ya hay un perfil administrador en esta empresa');
            return;
        }
    }
}
