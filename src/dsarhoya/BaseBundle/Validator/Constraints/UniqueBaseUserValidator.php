<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueBaseUserValidator extends ConstraintValidator
{
    private $em;
    private $user_class;
    
    public function __construct($entity_manager, $classes)
    {
        $this->em = $entity_manager;
        $this->user_class = $classes['user']['class'];
    }
    
    public function validate($user, Constraint $constraint)
    {
        /** @var \dsarhoya\BaseBundle\Entity\BaseUser $user */
        if (null === $user->getEmail()) {
            return;
        }

        $repo = $this->em->getRepository($this->user_class);
        $validated_users = $repo->findBy(array(
            'email'=>$user->getEmail(),
            'accountValidated'=>true
        ));
        
        foreach ($validated_users as $validated_user) {
            if ($validated_user->getId() == $user->getId()) {
                continue;
            }
            
            if (null !== $validated_user->getDeletedAt()) {
                continue; //El usuario está borrado
            }
            
            if (null !== $validated_user->getCompany() && null !== $validated_user->getCompany()->getDeletedAt()) {
                continue; //La empresa del usuario está borrada
            }
            
            if (is_null($validated_user->getDeletedAt())) {
                $this->context->addViolation('Ya hay un usuario en el sistema con ese email.');
                return ;
            }
        }
        
        $company_users = $repo->findBy(array(
            'email'=>$user->getEmail(),
            'company'=>$user->getCompany()->getId()
        ));
        foreach ($company_users as $company_user) {
            if ($company_user->getId() == $user->getId()) {
                continue;
            }
            
            if (is_null($company_user->getDeletedAt())) {
                $this->context->addViolation('Ya hay un usuario en esta empresa con ese email.');
                return ;
            }
        }
    }
}
