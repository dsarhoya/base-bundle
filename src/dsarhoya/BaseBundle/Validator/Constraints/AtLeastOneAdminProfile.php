<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AtLeastOneAdminProfile extends Constraint
{
    public $message = 'Debe existir al menos un usuario con perfil administrador';
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy() {
        return 'at_least_one_admin_profile_validator';
    }
}