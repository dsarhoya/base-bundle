<?php

namespace dsarhoya\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Description of newPHPClass
 *
 * @author matias
 */
class AdminBaseProfile extends Constraint{
    
    public $message = 'Debe existir un y solo un perfil de administrador';
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy() {
        return 'base_profile_validator';
    }
}
