<?php
/*
 * Configuración del voter:
 * 1. Dejarlo como servicio en el config.yml del proyecto:
   dsarhoya.authentication.customACLVoter:
        class:  dsarhoya\BaseBundle\Security\Authorization\Voter\CustomACLVoter
        arguments:  ["@service_container"]
        public:     false
        tags:
            - { name: security.voter }
 * 2. Cambiar la forma en que se toma la decición de los voters en security.yml del proyecto:
    security:
        access_decision_manager:
            # strategy can be: affirmative, unanimous or consensus
            strategy: unanimous
 * 3. Definir las reglas necesarias en la función vote
 */
namespace dsarhoya\BaseBundle\Security\Authorization\Voter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use dsarhoya\BaseBundle\Entity\CustomACLVoterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CustomACLVoter implements VoterInterface
{
    /* @var \Symfony\Component\HttpFoundation\RequestStack $requestStack */
    protected $requestStack;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container     = $container;
        $this->requestStack  = $requestStack;
    }

    public function supportsAttribute($attribute)
    {
        // you won't check against a user attribute, so return true
        return true;
    }

    public function supportsClass($class)
    {
        // your voter supports all type of token classes, so return true
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (null === $token) {
            $user =  null;
        }
        else{
            $user = $token->getUser();
        }

        //En este caso particular, editar incluye todo el CRUD
        if ($object instanceof CustomACLVoterInterface) {
            if($object->getCompany()->getId() != $user->getCompany()->getId()){
                return VoterInterface::ACCESS_DENIED;
            }
        }
        /* FUNCIONALIDAD DE EJEMPLO

        if ($this->urlMatches('/secured/user/cotizaciones/') && (!$user || !$user->isAllowed('COTIZACIONES'))) {
            return VoterInterface::ACCESS_DENIED;
        }
        */

        return VoterInterface::ACCESS_ABSTAIN;
    }

    protected function urlMatches($url_slice){
        return preg_match("/\b".str_replace("/","\/",$url_slice)."\b/i", $this->requestStack->getCurrentRequest()->getUri());
    }
}
