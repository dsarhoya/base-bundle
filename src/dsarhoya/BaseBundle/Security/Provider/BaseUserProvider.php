<?php

namespace dsarhoya\BaseBundle\Security\Provider;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use dsarhoya\BaseBundle\Entity\BaseUser;
use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\BaseBundle\Services\ParametersService;

/**
 * Description of BaseUserProvider
 *
 * @author mati
 */
class BaseUserProvider implements BaseUserProviderInterface{
    
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $classes;
    
    public function __construct(EntityManagerInterface $em, ParametersService $parametersService) {
        $this->em = $em;
        if ($parametersService->softDeleteable && is_null($this->em->getConfiguration()->getFilterClassName("soft-deleteable"))){
            $this->em->getConfiguration()->addFilter(
                'soft-deleteable',
                'Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter'
            );
            $this->em->getFilters()->enable('soft-deleteable');
        }
        
        $this->classes = $parametersService->classes;
    }
    
    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository($this->classes['user']['class'])->findOneBy([
            'email' => $username,
            'deletedAt' => null,
        ]);
        
        if($user){
            return $user;
        }
        
        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof BaseUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }
        
        return $this->em->getRepository($this->classes['user']['class'])->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->classes['user']['class'] === $class;
    }
}
