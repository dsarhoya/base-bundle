<?php

namespace dsarhoya\BaseBundle\Security\Provider;

use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Description of UserProviderInterface
 *
 * @author mati
 */
interface BaseUserProviderInterface extends UserProviderInterface
{
}
