<?php

namespace dsarhoya\BaseBundle\Security\Guard;

use Symfony\Component\Security\Core\User\UserInterface;

class NoPasswordLoginFormAuthenticator extends LoginFormAuthenticator
{
    public function checkCredentials($credentials, UserInterface $user)
    {
        //Si el UserProvider encontró el usuario, lo dejo pasar.
        return true;
    }
}
