<?php

namespace dsarhoya\BaseBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('dsarhoya_base');
        $rootNode = $treeBuilder->getRootNode();
        // $rootNode = $treeBuilder->root('dsarhoya_base');

        $rootNode
            ->children()
                ->arrayNode('routes')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('route')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('ACL')
                    ->children()
                        ->scalarNode('class')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('user_management_options')
                    ->children()
                        ->scalarNode('on_delete_check_admin_users')->defaultValue(true)->end()
                    ->end()
                ->end()
                ->arrayNode('email_service')
                    ->children()
                        ->enumNode('type')->values(['swift', 'ses', 'sendgrid'])->defaultValue('swift')->end()
                        ->scalarNode('ses_key')->end()
                        ->scalarNode('ses_secret')->end()
                        ->scalarNode('ses_region')->end()
                        ->scalarNode('from_email')->end()
                        ->scalarNode('sendgrid_apikey')->end()

                        ->arrayNode('catch_emails')
                            ->addDefaultsIfNotSet()
                            ->performNoDeepMerging()
                            ->children()
                                ->enumNode('enabled')->values([true, false])->defaultValue(false)->end()
                                ->arrayNode('to')
                                    ->beforeNormalization()
                                    ->ifString()
                                        ->then(function ($value) { return [$value]; })
                                    ->end()
                                    ->prototype('scalar')
                                    ->end()
                                ->end()
                                ->arrayNode('cc')
                                    ->beforeNormalization()
                                    ->ifString()
                                        ->then(function ($value) { return [$value]; })
                                    ->end()
                                    ->prototype('scalar')
                                    ->end()
                                ->end()
                                ->arrayNode('bcc')
                                    ->beforeNormalization()
                                    ->ifString()
                                        ->then(function ($value) { return [$value]; })
                                    ->end()
                                    ->prototype('scalar')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('monolog_email_handler')
                    ->children()

                        ->arrayNode('subject')
                            ->isRequired()
                            ->beforeNormalization()
                            ->ifString()
                                ->then(function ($value) { return ['default' => $value]; })
                            ->end()
                            ->prototype('scalar')
                            ->end()
                        ->end()

                        ->arrayNode('to')
                            ->isRequired()
                            ->beforeNormalization()
                            ->ifString()
                                ->then(function ($value) { return [$value]; })
                            ->end()
                            ->prototype('scalar')
                            ->end()
                        ->end()

                        ->scalarNode('template')->defaultValue('dsarhoyaBaseBundle:Monolog:message.email.twig')->end()

                    ->end()
                ->end()

                ->scalarNode('softDeleteable')->defaultValue(true)->end()
                ->scalarNode('uniqueUser')->defaultValue(true)->end()
                ->scalarNode('concurrentUsersListener')->defaultValue(false)->end()
                ->scalarNode('stickyUrls')->defaultValue(true)->end()
                ->scalarNode('serviceName')->isRequired()->end()
                ->arrayNode('classes')
                    ->children()
                        ->arrayNode('user')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                        ->arrayNode('company')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                        ->arrayNode('profile')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                        ->arrayNode('action')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                        ->arrayNode('notificationFolder')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('admin_menu_extension')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('route')->isRequired()->end()
                            ->scalarNode('display')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('stickyRoutes')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
