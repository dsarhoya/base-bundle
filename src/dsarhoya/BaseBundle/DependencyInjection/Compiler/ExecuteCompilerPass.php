<?php

namespace dsarhoya\BaseBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ExecuteCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // Antes había una lógica de ejecución de eventos. Se saca en favor del de Symofny.
    }
}
