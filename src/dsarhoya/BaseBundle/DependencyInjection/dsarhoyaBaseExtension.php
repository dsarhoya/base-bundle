<?php

namespace dsarhoya\BaseBundle\DependencyInjection;

use dsarhoya\BaseBundle\Listeners\ConcurrentUsersListener;
use dsarhoya\BaseBundle\Listeners\StickyUrlsListener;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class dsarhoyaBaseExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        //var_dump($config);

        $container->setParameter('dsarhoya_base.routes', $config['routes']);

        $container->setParameter('dsarhoya_base.ACL', isset($config['ACL']) ? $config['ACL'] : null);

        $this->addUserManagementOptions($container, $config);
        $this->addEmailServiceOptions($container, $config);

        if (!isset($config['classes'])) {
            $config['classes']['user'] = $this->addUserConfiguration();
            $config['classes']['company'] = $this->addCompanyConfiguration();
            $config['classes']['profile'] = $this->addProfileConfiguration();
            $config['classes']['action'] = $this->addActionConfiguration();
        }

        if (!isset($config['classes']['user'])) {
            $config['classes']['user'] = $this->addUserConfiguration();
        }

        if (!isset($config['classes']['company'])) {
            $config['classes']['company'] = $this->addCompanyConfiguration();
        }

        if (!isset($config['classes']['profile'])) {
            $config['classes']['profile'] = $this->addProfileConfiguration();
        }

        if (!isset($config['classes']['action'])) {
            $config['classes']['action'] = $this->addActionConfiguration();
        }

        $container->setParameter('dsarhoya_base.classes', $config['classes']);

        if (!isset($config['admin_menu_extension'])) {
            $admin_menu_extensions = [];
        } else {
            $admin_menu_extensions = $config['admin_menu_extension'];
        }

        $container->setParameter('dsarhoya_base.admin_menu_extension', $admin_menu_extensions);

        $container->setParameter('dsarhoya_base.softDeleteable', $config['softDeleteable']);
        $container->setParameter('dsarhoya_base.uniqueUser', $config['uniqueUser']);
        $container->setParameter('dsarhoya_base.serviceName', $config['serviceName']);

        if (true === $config['concurrentUsersListener']) {
            $this->addConcurrentListenerDefinition($container);
        }

        if (true === $config['stickyUrls']) {
            $this->addStickyUrlsDefinition($container);
        }

        if (!isset($config['stickyRoutes'])) {
            $stickyRoutes = [];
        } else {
            $stickyRoutes = $config['stickyRoutes'];
        }

        $container->setParameter('dsarhoya_base.stickyRoutes', $stickyRoutes);

        if (isset($config['monolog_email_handler'])) {
            $container->setParameter('dsarhoya_base.monolog_email_handler_parameters', $config['monolog_email_handler']);
        } else {
            $container->setParameter('dsarhoya_base.monolog_email_handler_parameters', []);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    private function addUserConfiguration()
    {
        return ['class' => 'dsarhoya\BaseBundle\Entity\User'];
    }

    private function addCompanyConfiguration()
    {
        return ['class' => 'dsarhoya\BaseBundle\Entity\Company'];
    }

    private function addProfileConfiguration()
    {
        return ['class' => 'dsarhoya\BaseBundle\Entity\Profile'];
    }

    private function addActionConfiguration()
    {
        return ['class' => 'dsarhoya\BaseBundle\Entity\Action'];
    }

    private function addUserManagementOptions(ContainerBuilder $container, $config)
    {
        $options = [];

        if (!isset($config['user_management_options'])) {
            $options['user_management_options'] = [
                'on_delete_check_admin_users' => true,
            ];
        } else {
            $options['user_management_options'] = $config['user_management_options'];
        }

        $container->setParameter('dsarhoya_base.user_management_options', $options['user_management_options']);
    }

    private function addEmailServiceOptions(ContainerBuilder $container, $config)
    {
        $options = [];

        if (!isset($config['email_service'])) {
            $options['email_service'] = [
                'type' => 'swift',
            ];
        } else {
            $options['email_service'] = $config['email_service'];
        }

        if (isset($config['email_service']['catch_emails'])) {
            $options['email_service']['catch_emails'] = $config['email_service']['catch_emails'];
        }

        $container->setParameter('dsarhoya_base.email_service', $options['email_service']);
    }

    private function addStickyUrlsDefinition(ContainerBuilder $container)
    {
        $definition = new Definition(StickyUrlsListener::class, [
            new Reference('router'),
            '%dsarhoya_base.stickyRoutes%'
        ]);

        $definition->addTag('kernel.event_listener', [
            'event' => 'kernel.request',
            'method' => 'onKernelRequest'
        ]);

        $container->setDefinition('dsarhoya.base.stickyurls', $definition);
    }

    private function addConcurrentListenerDefinition(ContainerBuilder $container)
    {
        $definition = new Definition(ConcurrentUsersListener::class, [
            new Reference('security.token_storage'),
            new Reference('security.authorization_checker'),
            new Reference('dsarhoya.base.userKeysService'),
            new Reference('router'),
            '%dsarhoya_base.uniqueUser%'
        ]);

        $definition->addTag('kernel.event_listener', [
            'event' => 'kernel.request',
            'method' => 'onKernelRequest',
            'priority' => 0,
        ]);

        $container->setDefinition('dsarhoya.base.concurrentUsersListener', $definition);
    }
}
