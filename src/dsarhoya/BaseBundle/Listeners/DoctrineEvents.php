<?php

namespace dsarhoya\BaseBundle\Listeners;

use Doctrine\ORM\Event\LifecycleEventArgs;
use dsarhoya\BaseBundle\Entity\BaseUser;
/*
 * Listeners para poner todo lo que queramos colgar desde un evento de Doctrine
 */

/**
 * Description of PreUpdate
 *
 * @author kito
 */
class DoctrineEvents {
    
    private $userClass;
    
    public function __construct($classes){
        $this->userClass = $classes['user']['class'];
    }
    
    //get calls before an update
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        //$entityManager = $args->getEntityManager();
        if ($entity instanceof $this->userClass) {
            $updateArray = $args->getEntityChangeSet();//get the modified fields
            if (array_key_exists("email", $updateArray) ){//if the email has changed, deactivate account
                $entity->setAccountValidated(BaseUser::ACCOUNT_NOT_VALIDATED);
            }
        }
        
    }
}
