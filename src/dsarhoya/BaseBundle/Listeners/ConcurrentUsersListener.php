<?php

namespace dsarhoya\BaseBundle\Listeners;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of ConcurrentUsersListener
 *
 * @author matias
 */
class ConcurrentUsersListener {
    
    private $tokenStorage;
    private $authorizationChecker;
    private $keysService;
    private $user;
    private $router;
    private $uniqueUser;
    
    public function __construct($tokenStorage, $authorizationChecker, $keysService, $router, $uniqueUser) {
        $this->tokenStorage  = $tokenStorage;
        $this->authorizationChecker  = $authorizationChecker;
        $this->keysService      = $keysService;
        $this->router           = $router;
        $this->uniqueUser       = $uniqueUser;
    }
    
    public function onKernelRequest(GetResponseEvent $event)
    {   
        if (null === $token = $this->tokenStorage->getToken()) {
            $this->user =  null;
        }
        else{
            $this->user = $token->getUser(); 
        }
        
        $this->keysService->setUser($this->user); //esto lo tengo que hacer para que el keysService siga funcionando..
        
        $request = $event->getRequest();
        $session = $request->getSession();
        
        if($this->user instanceof \dsarhoya\BaseBundle\Entity\BaseUser){ 
            
            if(is_string($this->user)) return;
            
            if($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')){//if the user is a super admin, we dont check
                return ; 
            }elseif($this->authorizationChecker->isGranted('ROLE_PREVIOUS_ADMIN')){//free raiders dont count, so we dont check
                return ;
            }elseif($this->user->isFreeRider()==1){//free raiders dont count, so we dont check
                return;
            }
            //ACCESS CODE CHECK
            if($this->router->generate('askAccessCode') == $request->getRequestUri()) return;
            else{
                if($this->user->getAccessCode()==1){
                    //the first time the listener is called, the key has not yet been created. thus, the session has no userKey
                    if(!$session->has('userKey')){
                        $event->setResponse(new RedirectResponse($this->router->generate('askAccessCode')));
                    }
                    elseif(!$this->keysService->isUsed($session->get('userKey'))){
                        $event->setResponse(new RedirectResponse($this->router->generate('askAccessCode')));
                    }
                }
            }
        }
        
        if($this->uniqueUser){//it is set to check for unique user
            if($this->router->generate('secured_index') == $request->getRequestUri()) return;

            if($this->user){
                if($session->has('userKey')){
                    if(!$this->keysService->checkConcurrentUser($session->get('userKey'))){
                        $event->setResponse(new RedirectResponse($this->router->generate('sessionOverride')));
                        
                    }
                }    
            }
        }
        if($this->user instanceof \dsarhoya\BaseBundle\Entity\BaseUser){
            //we check the max concurrent sessions
            $company = $this->user->getCompany();
            $maxConcurrentSessions = $company->getMaxConcurrentSessions();
            if(!is_null($maxConcurrentSessions)){//if is null, no limit is set
                if($this->user){
                    if($session->has('userKey')){
                        if(!$this->keysService->checkConcurrentSessions($session->get('userKey'),$company)){
                            $event->setResponse(new RedirectResponse($this->router->generate('sessionOverride')));
                        
                        }
                    }
                }

            }
            //update lastActivity
            $this->keysService->updateLastActivityByKey($session->get('userKey'));
        }

        return;
    }
}