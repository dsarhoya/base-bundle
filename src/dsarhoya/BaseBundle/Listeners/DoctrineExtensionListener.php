<?php
namespace dsarhoya\BaseBundle\Listeners;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DoctrineExtensionListener
{
    /**
     * @var ContainerInterface
     */
    protected $em;
    
    /**
     * @var boolean
     */
    protected $softDeleteable;

    public function setManager($manager, $softDeleteable){
        $this->em = $manager;
        $this->softDeleteable = $softDeleteable;
    }

    public function onKernelRequest(GetResponseEvent $event) 
    {
        //add the filter only it is has not been added before
        if ($this->softDeleteable && is_null($this->em->getConfiguration()->getFilterClassName("soft-deleteable"))){
            $this->em->getConfiguration()->addFilter(
                'soft-deleteable',
                'Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter'
            );
            $this->em->getFilters()->enable('soft-deleteable');
        }
    }
}