<?php

namespace dsarhoya\BaseBundle\Listeners;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of StickyUrlsListener
 *
 * @author matias
 */
class StickyUrlsListener {
    private $router;
    private $routes_to_match;
    
    public function __construct($router, $stickyRoutes) {
        $this->router           = $router;
        $this->routes_to_match = $stickyRoutes;
    }
    
    public function onKernelRequest(GetResponseEvent $event){
        
        $request = $event->getRequest();
        $session = $request->getSession();
        
        if(true){//if pensado para parametro del config.yml no implementado aún
            foreach ($this->routes_to_match as $route=>$regex) {
                if($session->has($route) && preg_match("/".str_replace('/', '\/', $regex)."/i", $request->getUri())){
                    $saved_left = explode('?', $session->get($route));
                    $current_left = explode('?', $request->getUri());
                    if($saved_left[0] == $current_left[0]){
                        $event->setResponse(new RedirectResponse($session->get($route)));  
                        $session->remove($route);
                    }
                }
            }
        }
    }
}