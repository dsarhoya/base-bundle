<?php 

namespace dsarhoya\BaseBundle\Model\Config;

/**
 * 
 */
interface BundleConfigInterface
{
    public function getServiceName();
}
