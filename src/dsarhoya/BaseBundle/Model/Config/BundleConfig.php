<?php 

namespace dsarhoya\BaseBundle\Model\Config;

use dsarhoya\BaseBundle\Model\Config\BundleConfigInterface;

/**
 * 
 */
class BundleConfig implements BundleConfigInterface
{
    
    private $serviceName;
    
    function __construct($serviceName)
    {
        $this->serviceName = $serviceName;
    }
    
    /**
     * Devuelve el nombre del servicio.
     * @return string nombre del servicio
     */
    public function getServiceName(){
        return $this->serviceName;
    }
    
}
