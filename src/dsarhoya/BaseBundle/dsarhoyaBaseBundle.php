<?php

namespace dsarhoya\BaseBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use dsarhoya\BaseBundle\DependencyInjection\Compiler\ExecuteCompilerPass;

class dsarhoyaBaseBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ExecuteCompilerPass());
    }
    
    public function boot() {
       parent::boot();
       
       $em = $this->container->get('doctrine.orm.entity_manager');
       $em->getConfiguration()->addCustomStringFunction('DSYDATEDIFFSECONDS', 'dsarhoya\BaseBundle\DQL\DSYDateDiffSeconds');
       $em->getConfiguration()->addCustomNumericFunction('RAND', 'dsarhoya\BaseBundle\DQL\DSYRand');
       $em->getConfiguration()->addCustomStringFunction('DSYMONTH', 'dsarhoya\BaseBundle\DQL\DSYMonth');
       $em->getConfiguration()->addCustomStringFunction('DSYYEAR', 'dsarhoya\BaseBundle\DQL\DSYYear');
       $em->getConfiguration()->addCustomStringFunction('DSYWEEK', 'dsarhoya\BaseBundle\DQL\DSYWeek');
       $em->getConfiguration()->addCustomStringFunction('DSYDAYOFWEEK', 'dsarhoya\BaseBundle\DQL\DSYDayOfWeek');
       $em->getConfiguration()->addCustomStringFunction('DSYSTRTODATE', 'dsarhoya\BaseBundle\DQL\DSYStrToDate');
       
    }
}
