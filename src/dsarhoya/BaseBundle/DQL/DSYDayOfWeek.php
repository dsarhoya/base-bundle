<?php

namespace dsarhoya\BaseBundle\DQL;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;

/**
 * year DQL function
 *
 * @author  kito
 * 
 */
class DSYDayOfWeek extends FunctionNode{
    protected $date = null;
    
    public function parse(\Doctrine\ORM\Query\Parser $parser){
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->date = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'DAYOFWEEK('.$this->date->dispatch($sqlWalker).')';
    }
}
?>
