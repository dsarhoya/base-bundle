<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 */
abstract class BaseAction
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $description;
    
    protected $title;

    /**
     * Constructor
     */
    public function __construct()
    {
    }
    
    /**
     * Set id
     *
     * @param string $id
     * @return Action
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Action
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @var string
     */
    protected $display;


    /**
     * Set display
     *
     * @param string $display
     * @return Action
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    
        return $this;
    }

    /**
     * Get display
     *
     * @return string 
     */
    public function getDisplay()
    {
        return $this->display;
    }
    
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }
    
    public function getTitle(){
        return $this->title;
    }
}
