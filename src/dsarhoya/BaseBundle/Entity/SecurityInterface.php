<?php

namespace dsarhoya\BaseBundle\Entity;

use dsarhoya\BaseBundle\Entity\BaseUser;

/**
 *
 * @author matias
 */
interface SecurityInterface {
    public function belongsTo(BaseUser $user);
}
