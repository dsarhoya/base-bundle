<?php

namespace dsarhoya\BaseBundle\Entity;

interface BaseNotifiableEventInterface {
    public function isNotifiable($date);
    public function getMessages();
}