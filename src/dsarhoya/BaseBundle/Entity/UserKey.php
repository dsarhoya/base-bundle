<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserKey
 */
class UserKey
{
    
    const END_RETRIEVE_PASSWORD = 'retrieve password';
    const END_VALIDATE_USER = 'validate user';
    const END_CONCURRENT_USER = 'concurrent user';
    const END_OTHER_ACTION = 'other action';
    
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \DateTime
     */
    private $expiration;
    
    /**
     * @var \dsarhoya\BaseBundle\Entity\BaseUserInterface
     */
    private $user;

    /**
     * @var string
     */
    private $privateKey;
    
    /**
     * @var boolean
     */
    private $used;

    /**
     * Constructor
     */
    public function __construct($end)
    {
        if(!in_array($end, self::getAvailableEnds())){
            throw new \Exception('You can use keys for supportend ends only: '.  implode(', ', self::getAvailableEnds()));
        } 
        
        $this->keyEnd = $end;
        $this->used = false;
        $this->creationDate = new \DateTime('now');
        $this->lastActivity = new \DateTime('now');
    }
    
    /**
     * Set expiration
     *
     * @param \DateTime $expiration
     * @return UserKey
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    
        return $this;
    }

    /**
     * Get expiration
     *
     * @return \DateTime 
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * Set user
     *
     * @param \dsarhoya\BaseBundle\Entity\BaseUserInterface $user
     * @return UserKey
     */
    public function setUser(\dsarhoya\BaseBundle\Entity\BaseUserInterface $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \dsarhoya\BaseBundle\Entity\BaseUserInterface 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set privateKey
     *
     * @param string $privateKey
     * @return UserKey
     */
    public function setPrivateKey($privateKey)
    {
        $this->privateKey = $privateKey;
    
        return $this;
    }

    /**
     * Get privateKey
     *
     * @return string 
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * Set used
     *
     * @param boolean $used
     * @return UserKey
     */
    public function setUsed($used)
    {
        $this->used = $used;
    
        return $this;
    }

    /**
     * Get used
     *
     * @return boolean 
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * Set end
     *
     * @param string $end
     * @return UserKey
     */
    public function setEnd($end)
    {
        $this->keyEnd = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return string 
     */
    public function getEnd()
    {
        return $this->keyEnd;
    }
    
    public static function getAvailableEnds(){
        $ends = array();
        $ends[] = self::END_RETRIEVE_PASSWORD;
        $ends[] = self::END_VALIDATE_USER;
        $ends[] = self::END_CONCURRENT_USER;
        $ends[] = self::END_OTHER_ACTION;
        return $ends;
    }
    /**
     * @var \DateTime
     */
    private $creationDate;


    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return UserKey
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    /**
     * @var string
     */
    private $keyEnd;


    /**
     * Set keyEnd
     *
     * @param string $keyEnd
     * @return UserKey
     */
    public function setKeyEnd($keyEnd)
    {
        $this->keyEnd = $keyEnd;

        return $this;
    }

    /**
     * Get keyEnd
     *
     * @return string 
     */
    public function getKeyEnd()
    {
        return $this->keyEnd;
    }
    /**
     * @var \DateTime
     */
    private $lastActivity;


    /**
     * Set lastActivity
     *
     * @param \DateTime $lastActivity
     * @return UserKey
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return \DateTime 
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }
}
