<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Profile
 */
abstract class BaseProfile
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    abstract public function getActions();
    
    public function __construct() {
        $this->setIsAdmin(false);
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Profile
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var boolean
     */
    protected $isAdmin;


    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     * @return BaseProfile
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean 
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }
}
