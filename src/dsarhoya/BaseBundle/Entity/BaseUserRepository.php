<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\EntityRepository;
use dsarhoya\BaseBundle\Entity\BaseUser;

/**
 * Description of newPHPClass
 *
 * @author matias
 */
class BaseUserRepository extends EntityRepository implements \Symfony\Component\Security\Core\User\UserProviderInterface
{
    public function otherAdminUser(BaseUser $user, $user_class){
        $qb = $this->createQueryBuilder('qb');
        $qb->add('select', 'u');
        $qb->add('from', "$user_class u");
        $qb->leftJoin('u.profile', 'p');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->neq('u.id', ':userId'));
        $andX->add($qb->expr()->eq('u.company', ':company'));
        $andX->add($qb->expr()->eq('p.isAdmin', ':isAdmin'));
        $qb->add('where', $andX);
        $qb->setParameters(array(
            'userId'=>$user->getId(),
            'company'=>$user->getCompany()->getId(),
            'isAdmin'=>true
        ));
        
        $res = $qb->getQuery()->getResult();
        if(count($res)==0){
            return null;
        }
        return $res[0];
    }
    
    public function adminUsers(BaseCompany $company, $user_class){
        
        $qb = $this->createQueryBuilder('qb');
        $qb->add('select', 'u');
        $qb->add('from', "$user_class u");
        $qb->leftJoin('u.profile', 'p');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('u.company', ':company'));
        $andX->add($qb->expr()->eq('p.isAdmin', ':isAdmin'));
        $qb->add('where', $andX);
        $qb->setParameters(array(
            'company'=>$company->getId(),
            'isAdmin'=>true
        ));
        
        return $qb->getQuery()->getResult();
    }
    
    public function loadUserByUsername($username)
    {
        $user = $this->createQueryBuilder('u')
            ->where('u.email = :email and u.deletedAt is null')
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
        
        if (null === $user) {
            $message = sprintf(
                'Unable to find an active admin AppBundle:User object identified by "%s".',
                $username
            );
            throw new \Symfony\Component\Security\Core\Exception\UsernameNotFoundException($message);
        }
        return $user;
    }

    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new \Symfony\Component\Security\Core\Exception\UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }
}
