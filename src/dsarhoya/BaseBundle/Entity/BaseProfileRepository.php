<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\EntityRepository;
use dsarhoya\BaseBundle\Entity\BaseProfile;

/**
 * Description of newPHPClass
 *
 * @author matias
 */
class BaseProfileRepository extends EntityRepository
{
    public function otherAdminProfile(BaseProfile $profile, $profile_class){
        $qb = $this->createQueryBuilder('qb');
        $qb->add('select', 'p');
        $qb->add('from', "$profile_class p");
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->neq('p.id', ':profileId'));
        $andX->add($qb->expr()->eq('p.company', ':company'));
        $andX->add($qb->expr()->eq('p.isAdmin', ':isAdmin'));
        $qb->add('where', $andX);
        $qb->setParameters(array(
            'profileId'=>$profile->getId(),
            'company'=>$profile->getCompany()->getId(),
            'isAdmin'=>true
        ));
        $res = $qb->getQuery()->getResult();
        if(count($res)==0){
            return null;
        }
        return $res[0];
    }
}
