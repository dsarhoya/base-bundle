<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 */
abstract class BaseCompany
{
    const ESTADO_ACTIVA = 1;
    const ESTADO_BLOQUEADA = 0;
    /**
     * @var integer
     */
    protected $id;
    
    /**
     * @var string
     */
    protected $rut;
    
    /**
     * @var string
     */
    protected $logo;
    
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $locality;

    /**
     * @var string
     */
    protected $notes;

    /**
     * @var integer
     */
    protected $state;
    
    public function __construct() {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set rut
     *
     * @param string $rut
     * @return Company
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    
        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Company
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set locality
     *
     * @param string $locality
     * @return Company
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
    
        return $this;
    }

    /**
     * Get locality
     *
     * @return string 
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Company
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Company
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }
    
    public function getStateString(){
        if($this->state==BaseCompany::ESTADO_ACTIVA){
            return "Activa";
        }elseif($this->state==BaseCompany::ESTADO_BLOQUEADA){
            return "Bloqueada";
        }
    }
    /**
     * @var integer
     */
    protected $maxConcurrentSessions;


    /**
     * Set maxConcurrentSessions
     *
     * @param integer $maxConcurrentSessions
     * @return BaseCompany
     */
    public function setMaxConcurrentSessions($maxConcurrentSessions)
    {
        $this->maxConcurrentSessions = $maxConcurrentSessions;

        return $this;
    }

    /**
     * Get maxConcurrentSessions
     *
     * @return integer 
     */
    public function getMaxConcurrentSessions()
    {
        return $this->maxConcurrentSessions;
    }
    /**
     * @var \DateTime
     */
    protected $deletedAt;


    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return BaseCompany
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
    /**
     * @var \DateTime
     */
    protected $createdAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BaseCompany
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var integer
     */
    protected $accountValidationLinkLifespan;


    /**
     * Set accountValidationLinkLifeSpan
     *
     * @param integer $accountValidationLinkLifeSpan
     * @return BaseCompany
     */
    public function setAccountValidationLinkLifespan($accountValidationLinkLifespan)
    {
        $this->accountValidationLinkLifespan = $accountValidationLinkLifespan;

        return $this;
    }

    /**
     * Get accountValidationLinkLifeSpan
     *
     * @return integer 
     */
    public function getAccountValidationLinkLifespan()
    {
        return is_null($this->accountValidationLinkLifespan)?2:$this->accountValidationLinkLifespan;
    }
}
