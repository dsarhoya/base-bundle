<?php

namespace dsarhoya\BaseBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * BaseUser.
 */
abstract class BaseUser implements UserInterface
{
    const ESTADO_ACTIVO = 1;
    const ESTADO_BLOQUEADO = 0;

    const ACCOUNT_VALIDATED = 1;
    const ACCOUNT_NOT_VALIDATED = 0;

    /**
     * @var int
     */
    protected $id;
    protected $profile;

    public function __construct()
    {
        $this->accountValidated = self::ACCOUNT_NOT_VALIDATED;
        $this->freeRaider = 0;
        $this->state = self::ESTADO_ACTIVO;
        $this->createdAt = new \DateTime('now');
        $this->password = '';
        $this->accessCode = 0;
        $this->emailSender = 0; //only designated users can den email to everyone
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     */
    protected $name;

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return BaseUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @var string
     */
    protected $password;

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return BaseUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @var string
     */
    protected $salt;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $state;

    /**
     * @var int
     */
    protected $accessCode;

    /**
     * Set salt.
     *
     * @param string $salt
     *
     * @return BaseUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt.
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return BaseUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set state.
     *
     * @param int $state
     *
     * @return BaseUser
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set accessCode.
     *
     * @param int $accessCode
     *
     * @return BaseUser
     */
    public function setAccessCode($accessCode)
    {
        $this->accessCode = $accessCode;

        return $this;
    }

    /**
     * Get accessCode.
     *
     * @return int
     */
    public function getAccessCode()
    {
        return $this->accessCode;
    }

    public function getUsername()
    {
        return $this->name;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials()
    {
    }

    public function isEnabled()
    {
        if ($this->getCompany() && ($this->getCompany()->getDeletedAt() || 0 === $this->getCompany()->getState())) {
            return false;
        }

        if (!$this->accountValidated) {
            return false;
        }

        if (!$this->state) {
            return false;
        }

        return true;
    }

    /*
        public function serialize()
        {
            return serialize($this->getId());
        }

        public function unserialize($data)
        {
            $this->id = unserialize($data);
        }
    */

    /**
     * Get isAllowed.
     */
    public function isAllowed($action)
    {
        if (!$this->getProfile()) {
            return false;
        }
        if ($this->getProfile()->getIsAdmin()) {
            return true;
        }

        $allowedActions = $this->getProfile()->getActions();

        foreach ($allowedActions as $allowedAction) {
            if ($allowedAction->getId() == $action) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get profile.
     *
     * @return \dsarhoya\TestAuthBundle\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @var int
     */
    protected $accountValidated;

    /**
     * Set accountValidated.
     *
     * @param bool $accountValidated
     *
     * @return BaseUser
     */
    public function setAccountValidated($accountValidated)
    {
        $this->accountValidated = $accountValidated;

        return $this;
    }

    /**
     * Get accountValidated.
     *
     * @return bool
     */
    public function getAccountValidated()
    {
        return $this->accountValidated;
    }

    /**
     * @var int
     */
    protected $freeRaider;

    /**
     * Set freeRaider
     * freeRiders dont generate a user key on login. One login can be used by several users
     * and dont count for the maxConcurrentSession of the company.
     *
     * @param int $freeRaider
     *
     * @return BaseUser
     */
    public function setFreeRaider($freeRaider)
    {
        $this->freeRaider = $freeRaider;

        return $this;
    }

    /**
     * Get freeRaider.
     *
     * @return int
     */
    public function getFreeRaider()
    {
        return $this->freeRaider;
    }

    public function isFreeRider()
    {
        return $this->getFreeRaider();
    }

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return BaseUser
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @var int
     */
    protected $emailSender;

    /**
     * Set emailSender.
     *
     * @param int $emailSender
     *
     * @return BaseUser
     */
    public function setEmailSender($emailSender)
    {
        $this->emailSender = $emailSender;

        return $this;
    }

    /**
     * Get emailSender.
     *
     * @return int
     */
    public function getEmailSender()
    {
        return $this->emailSender;
    }

    /**
     * @var \DateTime
     */
    protected $deletedAt;

    /**
     * Set deletedAt.
     *
     * @param \DateTime $deletedAt
     *
     * @return BaseUser
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
