<?php
/**
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

namespace dsarhoya\BaseBundle\Entity;

/**
 * Represents an enumerable set of values
 */
abstract class Enum
{
    /**
     * @var array A cache of all enum values to increase performance
     */
    protected static $keys_cache = array();
    protected static $values_cache = array();
    protected static $titles_cache = array();

    /**
     * Returns the names (or keys) of all of constants in the enum
     *
     * @return array
     */
    public static function keys()
    {   
        $class = get_called_class();

        if (!isset(self::$keys_cache[$class])) {
            self::setKeysAndValues();
        }

        return self::$keys_cache[$class];
    }

    /**
     * Return the names and values of all the constants in the enum
     *
     * @return array
     */
    public static function values()
    {
        $class = get_called_class();

        if (!isset(self::$values_cache[$class])) {
            self::setKeysAndValues();
        }

        return self::$values_cache[$class];
    }
    
    private static function setKeysAndValues(){
        $class = get_called_class();
        if (!isset(self::$keys_cache[$class])) {
            $reflected = new \ReflectionClass($class);
            $variables = $reflected->getStaticProperties();
            $keys = array();
            $values = array();
            foreach ($variables as $nombre => $valor) {
                if($nombre != 'keys_cache' && $nombre != 'values_cache' && $nombre != 'titles_cache' && substr($nombre, -5) != "_HELP" && substr($nombre, -6) != "_TITLE"){
                    $keys[] = $nombre;
                    $values[$nombre] = $valor;
                }
            }
            self::$keys_cache[$class] = $keys;
            self::$values_cache[$class] = $values;
        }
    }
    
    public static function titles(){
        $class = get_called_class();
        if (!isset(self::$titles_cache[$class])) {
            $reflected = new \ReflectionClass($class);
            $variables = $reflected->getStaticProperties();
            $titles = array();
            foreach ($variables as $nombre => $valor) {
                if(substr($nombre, -6) == "_TITLE"){
                    $titles[substr($nombre, 0, strlen($nombre)-6)] = $valor;
                }
            }
            $titles['Otros']='Otros';
            self::$titles_cache[$class] = $titles;
        }
        return self::$titles_cache[$class];
    }
    
    public static function explanationFor($id){
        return isset(static::${$id.'_HELP'})? static::${$id.'_HELP'} : null;        
    }
    
    public static function titleFor($id){
        //return preg_match("/\b".str_replace("/","\/",$url_slice)."\b/i", $request->getUri());
        foreach (self::titles() as $nombre => $title) {
            if(preg_match("/^$nombre?/", $id)){
                return $title;
            }
        }
        return 'Otros';
    }
}