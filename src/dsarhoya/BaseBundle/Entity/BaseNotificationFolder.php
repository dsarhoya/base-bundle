<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseNotificationFolder
 */
class BaseNotificationFolder
{
    /**
     * @var integer
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
