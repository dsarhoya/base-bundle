<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Option
 */
class Option
{
    
    CONST OPTION_ID_UNDER_MAINTENANCE = 'under_maintenance';
    CONST OPTION_ID_UNDER_MAINTENANCE_TITLE = 'under_maintenance_title';
    CONST OPTION_ID_UNDER_MAINTENANCE_MESSAGE = 'under_maintenance_message';
    
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    public function __construct($id, $value) {
        $this->id       = $id;
        $this->value    = $value;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Option
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Option
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    
    public static function getPrivateOptions(){
        $options = array();
        $options[] = self::OPTION_ID_UNDER_MAINTENANCE;
        $options[] = self::OPTION_ID_UNDER_MAINTENANCE_MESSAGE;
        $options[] = self::OPTION_ID_UNDER_MAINTENANCE_TITLE;
        return $options;
    }
}
