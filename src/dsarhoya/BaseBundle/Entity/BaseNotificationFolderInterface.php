<?php

namespace dsarhoya\BaseBundle\Entity;

interface BaseNotificationFolderInterface {
    public function getOwner();
}