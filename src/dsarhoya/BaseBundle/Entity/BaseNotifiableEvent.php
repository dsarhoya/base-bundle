<?php

namespace dsarhoya\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseNotifiableEvent
 */
class BaseNotifiableEvent
{
    const STATUS_PENDING = 'pending' ;
    const STATUS_SENT = 'sent';
    const STATUS_ERROR = 'error';
    const STATUS_DEACTVATED = 'deactivated';
    
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var string
     */
    protected $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return BaseNotifiableEvent
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BaseNotifiableEvent
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
