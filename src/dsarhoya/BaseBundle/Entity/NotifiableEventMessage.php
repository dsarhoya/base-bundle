<?php

namespace dsarhoya\BaseBundle\Entity;

class NotifiableEventMessage {
    private $addressee;
    private $cc;
    private $subject;
    private $bodyTemplate;
    private $bodyParameters;
    
    public function getAddressee(){
        return $this->addressee;
    }
    public function setAddressee($addressee){
        $this->addressee = $addressee;
        return $this->addressee;
    }
    public function getCC(){
        return $this->cc;
    }
    public function setCC($cc){
        $this->cc = $cc;
        return $this->cc;
    }
    public function getSubject(){
        return $this->subject;
    }
    public function setSubject($subject){
        $this->subject = $subject;
        return $this->subject;
    }
    public function getBodyTemplate(){
        return $this->bodyTemplate;
    }
    public function setBodyTemplate($bodyTemplate){
        $this->bodyTemplate = $bodyTemplate;
        return $this->bodyTemplate;
    }
    public function getBodyParameters(){
        return $this->bodyParameters;
    }
    public function setBodyParameters($bodyParameters){
        $this->bodyParameters = $bodyParameters;
        return $this->bodyParameters;
    }
}