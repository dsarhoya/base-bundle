<?php
namespace dsarhoya\BaseBundle\Entity;

use dsarhoya\BaseBundle\Entity\Enum;

/**
 * Contains enumerable CannedAcl values
 */
class CustomAcl extends Enum
{
    /*
     * Ejemplo:
     * 1. para poner un título, usar el sufijo _TITLE
     * 2. para poner una explicación, poner el sufijo _HELP
    
    protected static $COTIZACIONES = 'Entrar al módulo de cotizaciones';
    protected static $COTIZACIONES_TITLE = 'Modulo cotizaciones';
    protected static $COTIZACIONES_VER_DE_SUCURSAL = 'Ver las cotizaciones de toda la sucursal';
    protected static $COTIZACIONES_VER_DE_SUCURSAL_HELP = 'El usuario solo podrá ver las cotizaciones de la sucursal a la que pertenece';
     * 
     */
    
}
