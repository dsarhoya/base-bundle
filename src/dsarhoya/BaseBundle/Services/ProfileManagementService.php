<?php

namespace dsarhoya\BaseBundle\Services;

use dsarhoya\BaseBundle\Form\Type\ProfileType;

/**
 * Servicio para crear y editar usuarios en los proyectos sin tener que repetir la lógica
 *
 * @author kito
 */
class ProfileManagementService {
    
    private $em;
    private $formFactory;
    private $tokenStorage;
    private $profile_class;
    
    private $errors;
    
    public function __construct($entityManager, $formFactory, $tokenStorage, $classes){
        $this->em               = $entityManager;
        $this->formFactory      = $formFactory;
        $this->tokenStorage     = $tokenStorage;
        $this->profile_class    = $classes['profile']['class'];
        $this->errors           = array();
    }
    
    /*
     * Get form to create or update user
     */
    public function getForm($profile, $admin = false, $formType = null){
        
        if(is_null($formType)){
            $formType = ProfileType::class;
        }  
        
        return $this->formFactory->create(
            $formType,
            $profile, 
            array(
                'admin'=> $admin,
                'user' => $this->tokenStorage->getToken()->getUser()
                )
            );
    }
    
    public function persistProfile($profile){
        $this->em->persist($profile);
        $this->em->flush();
    }
    
    public function updateProfile($profile){
        $this->em->flush();
    }
    
    public function removeProfile($profile){
        if(!$profile->getIsAdmin()){
            $this->em->remove($profile);
            $this->em->flush();
            return true;
        }
        
        $repo = $this->em->getRepository('dsarhoya\BaseBundle\Entity\BaseProfile');
        $admin_profile = $repo->otherAdminProfile($profile, $this->profile_class);
        
        if(is_null($admin_profile)){
            $this->errors[] = 'No se puede borrar el perfil porque es el perfil de administrador';
            return false;
        }
        
        $this->em->remove($profile);
        $this->em->flush();
        return true;
    }
    
    public function getErrorsAsString(){
        return implode(', ', $this->errors);
    }
}
