<?php

namespace dsarhoya\BaseBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\BaseBundle\Entity\BaseUser;
use dsarhoya\BaseBundle\Entity\UserKey;
use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use dsarhoya\BaseBundle\Event\ValidationUrlEvent;
use dsarhoya\BaseBundle\Model\Config\BundleConfigInterface;
use dsarhoya\BaseBundle\Services\Options\BaseKeys\CreateKeyOptions;
use dsarhoya\BaseBundle\Services\Options\BaseKeys\SendAccountValidationEmailOptions;
use dsarhoya\BaseBundle\Services\Options\SendEmailOptions;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Templating\EngineInterface;
use Twig\Environment;

/**
 * Description of BaseKeysService.
 *
 * @author matias
 */
class BaseKeysService
{
    const ACCOUNT_VALIDATION_EMAIL_LIFESPAN = 2;

    private $errors = [];

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @var EmailService
     */
    private $emailService;

    private $serviceName;

    public function __construct(EntityManagerInterface $em, AuthorizationCheckerInterface $ac, TokenStorageInterface $ts, RouterInterface $r, EmailService $es, Environment $ei, EncoderFactoryInterface $efi, BundleConfigInterface $bundleConfig)
    {
        $this->em = $em;
        $this->authorizationChecker = $ac;
        $this->tokenStorage = $ts;
        $this->router = $r;
        $this->templating = $ei;
        $this->encoderFactory = $efi;
        $this->emailService = $es;
        $this->serviceName = $bundleConfig->getServiceName();
    }

    public function setEventDispatcher(EventDispatcherInterface $ed)
    {
        $this->eventDispatcher = $ed;
    }

    /**
     * @return BaseUser|null
     */
    private function getLoggedUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        return $token->getUser();
    }

    public function sendAccountValidationEmail(SendAccountValidationEmailOptions $options)
    {
        if (true === $options->checkLoggedUserPermissions) {
            if (!$this->hasSendAccountValidationEmailPermission($options->user)) {
                $this->errors[] = 'No tienes permisos para realizar esta operación';

                return false;
            }
        }

        $event = $this->eventDispatcher->dispatch(BaseBundleEvents::GENERATE_VALIDATION_URL, new ValidationUrlEvent($options->user));
        if (null === $url = $event->getUrl()) {
            $userKey = $this->createKey(new CreateKeyOptions([
                'user' => $options->user,
                'end' => UserKey::END_VALIDATE_USER,
                'privateKeySeed' => $options->user->getEmail(),
                'lifespan' => null !== $options->user->getCompany() ? $options->user->getCompany()->getAccountValidationLinkLifespan() : self::ACCOUNT_VALIDATION_EMAIL_LIFESPAN,
            ]));
            $url = $this->router->generate('validateAccount', ['key' => $userKey->getPrivateKey()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        if (null === $companyName = $options->companyName) {
            if (null === $company = $options->user->getCompany()) {
                $companyName = $company->getName();
            }
        }

        if (null === $serviceName = $options->serviceName) {
            $serviceName = $this->serviceName;
        }

        return $this->emailService->sendEmail(new SendEmailOptions([
            'to' => [$options->user->getEmail()],
            'subject' => 'Validar cuenta',
            'body' => $this->templating->render('@dsarhoyaBase/User/validateAccount.email.twig', ['url' => $url, 'companyName' => $companyName, 'serviceName' => $serviceName]),
        ]));
    }

    /**
     * Función para decidir si tengo o no permiso para mandar un mail de validación.
     * Esta función no tira una excepción si no existe el usuario o la empresa.
     *
     * @return bool
     */
    private function hasSendAccountValidationEmailPermission(BaseUser $toUser)
    {
        // Es importante el chequeo inicial, porque si no, el segundo lanza excepción.
        if (null !== $this->tokenStorage->getToken() && $this->authorizationChecker->isGranted(['ROLE_SUPER_ADMIN'])) {
            return true;
        }
        if (null === $this->getLoggedUser()) {
            return false;
        }
        if (1 === $this->getLoggedUser()->getEmailSender()) {
            return true;
        }
        if (null === $this->getLoggedUser()->getCompany()) {
            return false;
        }
        if (null === $toUser->getCompany()) {
            return false;
        }

        return (int) $toUser->getCompany()->getId() === $this->getLoggedUser()->getCompany()->getId();
    }

    public function createKey(CreateKeyOptions $options)
    {
        $userKey = new UserKey($options->end);
        $userKey->setUser($options->user);
        $userKey->setPrivateKey(md5(rand(1, 1000).$options->privateKeySeed.date('Y-m-d H:i:s')));
        $userKey->setExpiration(new \DateTime('+'.$options->lifespan.' day'));
        $this->em->persist($userKey);
        $this->em->flush($userKey);

        return $userKey;
    }

    public function setNewPassword($user, $pass)
    {
        $user->setSalt(md5(rand(1, 1000).$user->getEmail()));
        $encoder = $this->encoderFactory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($pass, $user->getSalt()));
    }

    public function getErrorsAsString()
    {
        if (count($this->errors)) {
            return implode(', ', $this->errors);
        } else {
            return 'No hay errores';
        }
    }
}
