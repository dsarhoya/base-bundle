<?php

namespace dsarhoya\BaseBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\BaseBundle\Entity\BaseProfile;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Description of PermissionsService
 *
 * @author mati
 */
class PermissionsService {
    
    private $em;
    private $parametersService;
    private $formFactory;
    
    public function __construct(
        ParametersService $parametersService,
        EntityManagerInterface $em,
        FormFactoryInterface $formFactory
    ) {
        $this->em = $em;
        $this->parametersService = $parametersService;
        $this->formFactory = $formFactory;
    }

    /**
     * @return type
     */
    public function getPermissionsForm(BaseProfile $profile, $actions = null){
        $allowed_actions = $profile->getActions();
        
        if (null === $actions){
            $actions = $this->getActions();
        }

        $this->setTitleForActions($actions);
        
        $defaults = array();
        foreach ($allowed_actions as $action) {
            $defaults[$action->getId()] = true;
        }
        $form_acciones_builder = $this->formFactory->createBuilder(FormType::class, $defaults);
        
        foreach ($actions as $accion) {
            $form_acciones_builder->add($accion->getId(), CheckboxType::class, array(
                'label'=>$accion->getDisplay(),
                'required'=>false,
                'attr'=>array('title'=>$accion->getTitle())
            ));
        }
        
        $form_acciones_builder->add('guardar', SubmitType::class);

        return $form_acciones_builder->getForm();
    }
    
    public function getActions(){
        if(!isset($this->parametersService->acl['class'])){
            throw new \Exception('Debes especificar la clas contenedora de los permisos');
        }
        $repoAction = $this->em->getRepository($this->parametersService->classes['action']['class']);
        $acciones = $repoAction->findAll();
        $this->setTitleForActions($acciones);
        return $acciones;
    }
    
    private function setTitleForActions(&$actions){
        $customAcl = new $this->parametersService->acl['class'];
        foreach ($actions as $accion) {
            $accion->setTitle($customAcl::titleFor($accion->getId()));
        }
    }
    
    public function setPermissions($profileId, $idsActions){
        $repoProfile = $this->em->getRepository($this->parametersService->classes['profile']['class']);
        $profile = $repoProfile->find($profileId);
        $repoAction = $this->em->getRepository($this->parametersService->classes['action']['class']);
        //first we delete unwanted permissions 
        
        $previouslyAllowedActions = array();
        foreach($profile->getActions() as $previousAction){
            $previouslyAllowedActions[] = $previousAction->getId();
        }
        
        $actionsToDelete = array_diff($previouslyAllowedActions, $idsActions);
        foreach($actionsToDelete as $actionToDelete){
            $profile->removeAction($repoAction->find($actionToDelete));
        }

        //we add new permissions
        $actionsToInclude = array_diff($idsActions,$previouslyAllowedActions);

        foreach($actionsToInclude as $actionToInclude){
            $profile->addAction($repoAction->find($actionToInclude));
        }
        $this->em->flush();
        return $profile->getActions();
    }

    public function getActionTitles($actions = null){
        
        if(!isset($this->parametersService->acl['class'])){
            throw new \Exception('Debes especificar la clas contenedora de los permisos');
        }

        $customAcl = new $this->parametersService->acl['class'];
        
        if ( is_null($actions) ){
            return $customAcl::titles();
        }
        $titles = [];
        foreach( $actions as $action ){
            $title = $customAcl::titleFor($action->getId());
            if ( !in_array($title, $titles) ){
                $titles[$action->getId()] = $customAcl::titleFor($action->getId());
            }
        }
        $titles['Otros']='Otros';
        return $titles;
    }
}
