<?php

namespace dsarhoya\BaseBundle\Services;

use dsarhoya\BaseBundle\Entity\BaseUser;
use dsarhoya\BaseBundle\Entity\UserKey;
use dsarhoya\BaseBundle\Entity\BaseCompany;
use dsarhoya\BaseBundle\Services\Options\SendEmailOptions;
use dsarhoya\BaseBundle\Services\Options\BaseKeys\SendAccountValidationEmailOptions;
use dsarhoya\BaseBundle\Services\BaseKeysService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use dsarhoya\BaseBundle\Form\Type\User\ChangePasswordType;
use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use dsarhoya\BaseBundle\Event\ValidationUrlEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of UserKeysService
 *
 * @author matias
 */
class UserKeysService {
    
    private $em;
    private $user;
    private $router;
    private $templating;
    private $mailer;
    private $encoderFactory;
    
    /**
     * @var \Symfony\Component\Form\FormFactory
     */
    private $formFactory;
    private $validator;
    private $errors = array();
    private $tokenStorage;
    private $authorizationChecker;
    private $serviceName;
    /**
     * @var EmailService
     */
    private $emailsService;
    
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;


    /* News services */
    /**
     * @var \dsarhoya\aseBundle\Services\BaseKeysService
     */
    private $baseKeysService;

    public function __construct($entityManager, $tokenStorage, $authorizationChecker, $router, $mailer, $encoderFactory, $formFactory, $validator, $serviceName, $emails, $eventDispatcher, BaseKeysService $baseKeysService) {

        
        $this->em               = $entityManager;
        $this->router           = $router;
        $this->mailer           = $mailer;
        $this->encoderFactory   = $encoderFactory;
        $this->formFactory      = $formFactory;
        $this->validator        = $validator;
        $this->tokenStorage  = $tokenStorage;
        $this->authorizationChecker  = $authorizationChecker;
        $this->serviceName      = $serviceName;
        $this->emailsService    = $emails;
        $this->eventDispatcher  = $eventDispatcher;
        
        if (null === $token = $this->tokenStorage->getToken()) {
            $this->user =  null;
        }
        else{
            $this->user = $token->getUser(); 
        }
        
        /* New services */
        
        $this->baseKeysService = $baseKeysService;
        
    }

    public function setTemplating($templating){
        $this->templating      = $templating;
    }
    
    public function setUser($user){
        $this->user = $user;
    }
    
    /*
     * RESET PASSWORD
     */
    public function sendResetPasswordKey(BaseUser $user){
        
        $user_key = new UserKey(UserKey::END_RETRIEVE_PASSWORD);
        $user_key->setUser($user);
        $user_key->setPrivateKey(md5(rand(1,1000).$user->getEmail().date("Y-m-d H:i:s")));
        $user_key->setExpiration(new \DateTime('+1 hour'));

        $this->em->persist($user_key);
        $this->em->flush();

        $url = $this->router->generate('retrievePassword', array('key'=>$user_key->getPrivateKey()), UrlGeneratorInterface::ABSOLUTE_URL);
        
        return $this->emailsService->sendEmail(new SendEmailOptions([
            'to' => [$user->getEmail()],
            'subject' => 'Recuperar contraseña',
            'body' => $this->templating->render('@dsarhoyaBase/User/retrievePassword.email.twig', array('url'=>$url))
        ]));
        
    }
    
    public function validateKey($key, $end){
        if(!in_array($end, UserKey::getAvailableEnds())){ 
            $this->errors[] = 'La llave para realizar esta operación es inválida';
            return false;
        }
        
        if(!($key instanceof UserKey)){
            $repo = $this->em->getRepository("dsarhoyaBaseBundle:UserKey");
            $user_key = $repo->findOneByPrivateKey($key);
        }else{
            $user_key = $key;
        }
        
        /* @var $user_key UserKey */
        if(!$user_key){
            $this->errors[] = 'No se encontró la llave para realizar esta operación';
            return false;
        }
        
        if($user_key->getEnd() != $end){ 
            $this->errors[] = 'La llave para realizar esta operación es inválida';
            return false;
        }
        
        $now = new \DateTime('NOW');
        
        if($now > $user_key->getExpiration()){
            $this->errors[] = 'Esta llave ha expirado';
            return false;
        }
        
        if($user_key->getUsed()){
            $this->errors[] = 'Esta llave ya fue usada';
            return false;
        }
        
        return $user_key;
    }
    
    public function getChangePasswordForm($key, $end){
        
        if(!$this->validateKey($key, $end)){
            $this->errors[] = 'Llave inválida';
            return false;
        }
        
        $action = $end == UserKey::END_RETRIEVE_PASSWORD ? 
                $this->router->generate('retrievePassword', array('key'=>$key->getPrivateKey())) : 
                $this->router->generate('validateAccount', array('key'=>$key->getPrivateKey())) ;
        
        $form = $this->formFactory->create(ChangePasswordType::class, null, [
            'action' => $action
        ]);
        
        return $form;
    }
    
    public function changePassword($key, $data){
        
        $user_key = $this->validateKey($key, UserKey::END_RETRIEVE_PASSWORD);
        if(!$user_key){
            $this->errors[] = 'No se encontró la llave para realizar esta operación';
            return false;
        }
        if(!$this->generalChangePassword($user_key, $data)){
            //No pongo errores porque estos ya estan seteados en la funcion para cambiar contraseña.
            return false;
        }
        //si la cuenta no estaba validada, la validamos
        $user_key->getUser()->setAccountValidated(true);
        $user_key->setUsed(true);
        $this->em->flush();
        
        return true;
    }
    
    private function generalChangePassword($user_key, $data){
        
        if(!$user_key){
            $this->errors[] = 'No se encontró la llave para realizar esta operación';
            return false;;
        }
        if(!$data){
            $this->errors[] = 'Ocurrió un error inesperado';
            return false;
        }
        if(!isset($data['pass'])){
            $this->errors[] = 'Ocurrió un error inesperado';
            return false;
        }
        if(!isset($data['repeatedPass'])){
            $this->errors[] = 'Ocurrió un error inesperado';
            return false;
        }
        
        if($data['pass'] != $data['repeatedPass']){
            $this->errors[] = 'Las contraseñas deben coincidir';
            return false;
        }
        if(strlen($data['pass']) < 6){
            $this->errors[] = 'La contraseña debe tener al menos 6 caracteres';
            return false;
        }
        
        $this->setNewPassword($user_key->getUser(), $data['pass']);
        
        return true;

    }
    
    private function setNewPassword($user, $pass){
        @trigger_error('DSY función deprecada, llamar directo al BaseKeysService', E_USER_DEPRECATED);
        $this->baseKeysService->setNewPassword($user, $pass);
    }
    
    /**
     * BC_CHANGE ya no se puede llamar a esta función sin usuario.
     * BC_CHANGE Ahora no se revisa por defecto que se tenga permiso.
     * @param  [type] $user        [description]
     * @param  [type] $companyName [description]
     * @param  [type] $serviceName [description]
     * @return [type]              [description]
     */
    public function sendAccountValidationEmail(BaseUser $user = null, $companyName=null, $serviceName=null){
        @trigger_error('DSY función deprecada, llamar directo al BaseKeysService', E_USER_DEPRECATED);
        return $this->baseKeysService->sendAccountValidationEmail(new SendAccountValidationEmailOptions([
            'user' => $user
        ]));
    }
    
    public function validateUser($key, $data, $userClass){
        
        $user_key = $this->validateKey($key, UserKey::END_VALIDATE_USER);
        if(!$user_key){
            $this->errors[] = 'No se encontró la llave para realizar esta operación';
            return false;
        }
        if(!$this->generalChangePassword($user_key, $data)) return false;
        
        /* @var $user BaseUser */
        $user = $user_key->getUser();
        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        if(!$repo->existeUsuarioValidado($user->getEmail(), $userClass)){
            $user->setAccountValidated(true);
            $user_key->setUsed(true);
        }else{
            $this->errors[] = 'Ya existe un usuario validado con este correo.';
            return false;
        }
        $this->em->flush();
        return true;
    }
    
    public function getErrorsAsString(){
        if(count($this->errors))
            return implode(', ', $this->errors);
        else
            return 'No hay errores';
    }
    
    public function checkConcurrentUser($userKey){
        //return true: esta todo bien
        //return false: hay más usuarios de lo permitido.

        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        
        $key = $repo->userConcurrentKey($this->user->getId());
        
        if($key->getPrivateKey() == $userKey) return true;
        
        return false; 
    }
    
    public function newConcurrentUserKey(BaseUser $user){
        
        $key = new UserKey(UserKey::END_CONCURRENT_USER);
        $key->setUser($user);
        $expiration = new \DateTime('now');
        $key->setExpiration($expiration->add(new \DateInterval('P1D')));
        $key->setPrivateKey(md5($user->getId().'-'.$key->getCreationDate()->format('U')));
        //if the user doesn't require to provide an access code
        //we define that the access key has been already used.
        //Later in the ConcurrentUsersListener.php we check if is the has 
        //been used, to require the access code or not
        if($user->getAccessCode()==0){
            $key->setUsed(1);
        }
        $this->em->persist($key);
        $this->em->flush($key);
        
        return $key;
    }
    
    public function checkConcurrentSessions($userKey, BaseCompany $company){
        //return true: esta todo bien
        //return false: hay más sesiones de lo permitido.

        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        
        $maxConcurrentSessions = $company->getMaxConcurrentSessions();
        
        $keys = $repo->getActiveKeysByCompany($company->getId(),$maxConcurrentSessions);
        
        foreach($keys as $key){
            if($key->getPrivateKey() == $userKey){
                return true;
            }
        }
        return false;
    }
    
    public function updateLastActivityByKey($key){
        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        $userKey = $repo->findOneBy(array('privateKey'=>$key));
        if($userKey){
            $userKey->setLastActivity(new \DateTime("now"));
            $this->em->flush($userKey);
        }
        return true;
    }
    
    public function revokeUserKey($key){
        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        $userKey = $repo->findOneBy(array('privateKey'=>$key));
        if($userKey){
            $this->em->remove($userKey);
            $this->em->flush($userKey);
        }
        return true;
    }
    
    public function isUsed($key){
        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        $userKey = $repo->findOneBy(array('privateKey'=>$key));
        if(!$userKey){//if the user was logged out by the system the key gets deleted
            return false;
        }
        return $userKey->getUsed()==1;
    }
    
    public function checkAccessCode($accessCode, $key){
        $repo = $this->em->getRepository('dsarhoyaBaseBundle:UserKey');
        $userKey = $repo->findOneBy(array('privateKey'=>$key));
        if($accessCode==$userKey->getId()){
            $userKey->setUsed(1);
            $this->em->flush($userKey);
            return true;
        }else{
            return false;
        }
    }
    
    public function sendAccessCodeEmail(UserKey $userKey){
        $user = $userKey->getUser();
        $email = \Swift_Message::newInstance()
            ->setSubject($this->serviceName.' - Código de acceso')
            ->setFrom($this->mailerUser)
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    '@dsarhoyaBase/User/accessCode.email.twig', 
                        array(
                            'accessCode'=>$userKey->getId(),
                            'serviceName'=>$this->serviceName
                            )
                ),
                'text/html'
            )
        ;
        
        return $this->mailer->send($email);
    }
}
