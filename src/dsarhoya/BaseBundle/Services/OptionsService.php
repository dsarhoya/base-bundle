<?php

namespace dsarhoya\BaseBundle\Services;

use dsarhoya\BaseBundle\Entity\Option;

/**
 * Description of UserKeysService
 *
 * @author matias
 */
class OptionsService {
    
    private $em;
    private $or;
    private $errors;
    
    public function __construct($entityManager) {
        $this->em               = $entityManager;
        $this->or               = $this->em->getRepository('dsarhoyaBaseBundle:Option');
        $this->errors           = array();
    }
    
    public function getErrorsAsString(){
        return implode(', ', $this->errors);
    }
    
    public function setMaintenanceState($title, $message){
        
        if($this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE)){
            $this->errors[] = 'Maintenance state already set';
            return false;
        }
        
        $option = new Option(Option::OPTION_ID_UNDER_MAINTENANCE, true);
        $this->em->persist($option);
        
        $title = new Option(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE, $title);
        $this->em->persist($title);
        
        $message = new Option(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE, $message);
        $this->em->persist($message);
        
        $this->em->flush();
        
        return true;
        
    }
    
    public function getMaintenanceState(){
        if(!$this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE)){
            $this->errors[] = 'Maintenance state not set';
            return false;
        }
        
        $title = $this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE);
        $message = $this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE);
        
        return array(
            Option::OPTION_ID_UNDER_MAINTENANCE_TITLE => $title ? $title->getValue() : 'No title',
            Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE => $message ? $message->getValue() : 'No message'
        );
    }
    
    public function removeMaintenanceState(){
        if(!$this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE)){
            $this->errors[] = 'Maintenance state not set';
            return false;
        }
        
        $option = $this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE);
        $title = $this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE);
        $message = $this->or->find(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE);
        
        if($option) {$this->em->remove($option);}
        if($title) {$this->em->remove($title);}
        if($message) {$this->em->remove($message);}
        
        $this->em->flush();
        
        return true;
    }
    
    public function setOption($id, $value){
        if(in_array($id, Option::getPrivateOptions())){
            throw new \Exception(sprintf('No se puede sobre escribir la opción %s', $id));
        }
        if(!is_string($value)){
            throw new \Exception(sprintf('Las opciones solo pueden ser de tipo string'));
        }
        $option = $this->or->find($id);
        if(is_null($option)){
            $option = new Option($id, $value);
        }
        
        $option->setValue($value);
        $this->em->persist($option);
        $this->em->flush();
        
        return true;
    }
    
    public function getOption($id){
        if(in_array($id, Option::getPrivateOptions())){
            throw new \Exception(sprintf('No se puede obtener la opción %s', $id));
        }
        $option = $this->or->find($id);
        return $option;
    }
    
    public function removeOption($id){
        if(in_array($id, Option::getPrivateOptions())){
            throw new \Exception(sprintf('No se puede eliminar la opción %s', $id));
        }
        
        $option = $this->or->find($id);
        if(is_null($option)){
            $this->errors[] = sprintf('La opción %s no existe', $id); 
            return false;
        }
        
        $this->em->remove($option);
        $this->em->flush();
        
        return true;
    }
}
