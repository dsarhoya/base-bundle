<?php

namespace dsarhoya\BaseBundle\Services;

/**
 * Description of ParametersService
 *
 * @author mati
 */
class ParametersService {
    
    public $classes;
    public $routes;
    public $uniqueUser;
    public $softDeleteable;
    public $adminMenuExtension;
    public $acl;
    
    public function __construct(
        array $classes, 
        array $routes,
        $uniqueUser,
        $softDeleteable,
        $adminMenuExtension,
        $acl
    ) {
        $this->classes = $classes;
        $this->routes = $routes;
        $this->uniqueUser = $uniqueUser;
        $this->softDeleteable = $softDeleteable;
        $this->adminMenuExtension = $adminMenuExtension;
        $this->acl = $acl;
    }
}
