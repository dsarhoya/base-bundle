<?php

namespace dsarhoya\BaseBundle\Services\Monolog\Handlers;

use dsarhoya\BaseBundle\Services\EmailService;
use dsarhoya\BaseBundle\Services\Options\SendEmailOptions;
use Monolog\Handler\MailHandler as MonologMailHandler;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class MailHandler extends MonologMailHandler
{
    private $emailService;
    private $templating;
    private $emailHandlerParameters;

    public function __construct(EmailService $emailService, EngineInterface $templating, array $emailHandlerParameters)
    {
        $this->emailService = $emailService;
        $this->templating = $templating;
        $this->emailHandlerParameters = $emailHandlerParameters;
    }

    protected function send($content, array $records)
    {
        $record = array_shift($records);

        $subject = $this->emailHandlerParameters['subject']['default'];

        if (in_array($record['channel'], array_keys($this->emailHandlerParameters['subject']))) {
            $subject = $this->emailHandlerParameters['subject'][$record['channel']];
        }
        
        $params = [
            'message' => $record['message'],
            'datetime' => $record['datetime'],
            'subject' => $subject,
        ];
        
        $this->emailService->sendEmail(new SendEmailOptions([
            'to' => $this->emailHandlerParameters['to'],
            'subject' => $subject,
            'body' => $this->templating->render($this->emailHandlerParameters['template'], $params),
        ]));
    }
}
