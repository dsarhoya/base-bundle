<?php

namespace dsarhoya\BaseBundle\Services;

use dsarhoya\BaseBundle\Services\Options\SendEmailOptions;

/**
 * Description of EmailService.
 *
 * @author matias
 */
class EmailService
{
    private $config;
    private $mailer;
    private $mailer_user;

    public function __construct($config, $mailer, $mailer_user)
    {
        $this->config = $config;
        $this->mailer = $mailer;
        $this->mailer_user = $mailer_user;
    }

    public function sendEmail(SendEmailOptions $options)
    {
        if ('swift' === $this->config['type']) {
            return $this->sendSwiftEmail($options);
        } elseif ('ses' === $this->config['type']) {
            return $this->sendSESEmail($options);
        } elseif ('sendgrid' === $this->config['type']) {
            return $this->sendSendgridEmail($options);
        } else {
            throw new \Exception(sprintf('Servicio de envío de correo "%s" no reconocido', $this->config['type']));
        }
    }

    /**
     * Esta función deduce el from del email con la siguiente prioridad:
     * 1. El que está definido en las opciones.
     * 2. El que está definido en el config.yml
     * 3. El que está definido en swiftmailer.username
     * 4. El mail de contacto de dsarhoya.
     *
     * @return string
     */
    private function from(SendEmailOptions $options)
    {
        if (null !== $options->from()) {
            return $options->from();
        }
        if (isset($this->config['from_email'])) {
            return $this->config['from_email'];
        }
        if (null !== $this->mailer_user) {
            return $this->mailer_user;
        }

        return 'contacto@dsarhoya.cl';
    }

    private function to(SendEmailOptions $options)
    {
        if (false === $this->config['catch_emails']['enabled']) {
            return $options->to();
        }

        return $this->config['catch_emails']['to'];
    }

    private function cc(SendEmailOptions $options)
    {
        if (false === $this->config['catch_emails']['enabled']) {
            return $options->cc();
        }

        return $this->config['catch_emails']['cc'];
    }

    private function bcc(SendEmailOptions $options)
    {
        if (false === $this->config['catch_emails']['enabled']) {
            return $options->bcc();
        }

        return $this->config['catch_emails']['bcc'];
    }

    public function sendSwiftEmail(SendEmailOptions $options)
    {
        $email = (new \Swift_Message($options->subject()))
            ->setBody($options->body(), 'text/html')
            ->setFrom($this->from($options), $options->fromName())
        ;

        if (is_array($options->to())) {
            foreach ($this->to($options) as $index => $value) {
                $email->addTo(is_string($index) ? $index : $value,
                        is_string($index) ? $value : null
                        );
            }
        }

        if (is_array($options->cc())) {
            foreach ($this->cc($options) as $index => $value) {
                $email->addCc(is_string($index) ? $index : $value,
                        is_string($index) ? $value : null
                        );
            }
        }

        if (is_array($options->bcc())) {
            foreach ($this->bcc($options) as $index => $value) {
                $email->addBcc(is_string($index) ? $index : $value,
                        is_string($index) ? $value : null
                        );
            }
        }

        return $this->mailer->send($email) ? true : false;
    }

    /**
     * @todo: falta probar esto.
     */
    public function sendSESEmail(SendEmailOptions $options)
    {
        $toAddresses = [];
        $ccAddresses = [];
        $bccAddresses = [];

        foreach ($this->to($options) as $index => $value) {
            $toAddresses[] = is_string($index) ? sprintf("=?utf-8?B?%s?= <$index>", $value) : $value;
        }

        if (is_array($options->cc())) {
            foreach ($this->cc($options) as $index => $value) {
                $ccAddresses[] = is_string($index) ? sprintf("=?utf-8?B?%s?= <$index>", $value) : $value;
            }
        }

        if (is_array($options->bcc())) {
            foreach ($this->bcc($options) as $index => $value) {
                $bccAddresses[] = is_string($index) ? sprintf("=?utf-8?B?%s?= <$index>", $value) : $value;
            }
        }

        $source = $options->fromName() ? sprintf('=?utf-8?B?%s?= <%s>', base64_encode($options->fromName()), $this->from($options)) : $this->from($options);

        $parameters = [
            'Source' => $source,
            'Destination' => [
                'ToAddresses' => $toAddresses,
                'CcAddresses' => $ccAddresses,
                'BccAddresses' => $bccAddresses,
                ],
            'ReturnPath' => $this->from($options),
            'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => 'UTF-8',
                        'Data' => $options->body(),
                        ],
                    ],
                'Subject' => [
                    'Charset' => 'UTF-8',
                    'Data' => $options->subject(), ], ],
            ];

        /* @var $res \Aws\Result */
        try {
            $res = $this->getSesClient()->sendEmail($parameters)->toArray();
        } catch (\Aws\Ses\Exception\SesException $ex) {
            return false;
        }

        if (isset($res['@metadata']) &&
            isset($res['@metadata']['statusCode']) &&
            (200 === $res['@metadata']['statusCode'])) {
            return true;
        }

        return false;
    }

    /**
     * @return \Aws\Ses\SesClient
     */
    private function getSesClient()
    {
        return \Aws\Ses\SesClient::factory([
            'version' => 'latest',
            'region' => $this->config['ses_region'],
            'credentials' => [
                'key' => $this->config['ses_key'],
                'secret' => $this->config['ses_secret'],
            ],
        ]);
    }

    /**
     * @throws Exception
     */
    public function sendSendgridEmail(SendEmailOptions $options)
    {
        if (null == $this->config['sendgrid_apikey']) {
            throw new Exception('Missing required parameter sendgrid_apikey');
        }

        $sendOk = true;
        foreach ($this->to($options) as $receipent) {
            $from = new \SendGrid\Email($options->fromName(), $options->from());
            $subject = $options->subject();
            $to = new \SendGrid\Email('', $receipent);
            $content = new \SendGrid\Content('text/html', $options->body());
            $mail = new \SendGrid\Mail($from, $subject, $to, $content);
            $apiKey = $this->config['sendgrid_apikey'];
            $sg = new \SendGrid($apiKey);
            $response = $sg->client->mail()->send()->post($mail);
            $sendOk = 200 == $response->statusCode();
        }

        return $sendOk;
    }
}
