<?php

namespace dsarhoya\BaseBundle\Services;

use dsarhoya\BaseBundle\Entity\BaseUser;
use dsarhoya\BaseBundle\Options\UserManagementDeleteOptions;
use dsarhoya\BaseBundle\Form\Type\UserType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use dsarhoya\BaseBundle\Services\BaseKeysService;
use dsarhoya\BaseBundle\Services\Options\BaseKeys\SendAccountValidationEmailOptions;

/**
 * Servicio para crear y editar usuarios en los proyectos sin tener que repetir la lógica
 *
 * @author kito
 */
class UserManagementService {
    
    private $em;
    private $encoderFactory;
    private $userKeysService;
    private $formFactory;
    private $tokenStorage;
    private $authorizationChecker;
    private $userClass;
    private $options;
    
    private $errors;
    
    /* Nuevos servicios */
    
    /**
     * @var BaseKeysService
     */
    private $baseKeysService;
    
    public function __construct($entityManager, $encoderFactory, $userKeysService, $formFactory, $tokenStorage, $authorizationChecker, $classes, $options, $baseKeysService){
        $this->em               = $entityManager;
        $this->encoderFactory   = $encoderFactory;
        $this->userKeysService  = $userKeysService;
        $this->formFactory      = $formFactory;
        $this->tokenStorage     = $tokenStorage;
        $this->authorizationChecker  = $authorizationChecker;
        $this->userClass        = $classes['user']['class'];
        $this->options          = $options;
        $this->errors           = array();
        $this->baseKeysService = $baseKeysService;
    }
    
    /*
     * Get form to create or update user
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm($user,$baseFieldsToInclude, $admin = false, $formType = null, $company = null, $additionalFieldsToInclude = array(), $options = []){
        if(is_null($formType)){
            $formType = UserType::class;
        }  
        if(is_null($company)){
            $company = $user->getCompany();
        }
        
        return $this->formFactory->create(
            $formType,
            $user, 
            array_merge(
                [
                    'admin'=>$admin,
                    'company'   => $company,
                    "baseFieldsToInclude"=>$baseFieldsToInclude,
                    "additionalFieldsToInclude" => $additionalFieldsToInclude

                ],
                $options
                )
            );
    }
    
    /*
     * Persist user in the data base
     * BC_CHANGE esto por defecto no valida el permiso del usuario logueado para enviar correos
     */
    public function persistUser($user, $companyName=null, $serviceName=null, $checkLoggedUserPermissions = false){
        
        $user->setSalt(md5(rand(1,1000).$user->getEmail()));
        // depende de si necesito o no validacion de cuenta.
        /* @var $user BaseUser */
        if(!$user->getAccountValidated()){
            $this->baseKeysService->sendAccountValidationEmail(new SendAccountValidationEmailOptions([
            'user' => $user, 'companyName' => $companyName, 'serviceName' => $serviceName, 'checkLoggedUserPermissions' => $checkLoggedUserPermissions
        ]));
        }else{
            $encoder = $this->encoderFactory->getEncoder($user);//we need to encode the password
            $user->setPassword($encoder->encodePassword($user->getPassword(),$user->getSalt()));
        }
        $this->em->persist($user);
        $this->em->flush();
        
        return true;
    }
    
    /*
     * Update user in the data base 
     * BC_CHANGE esto por defecto no valida el permiso del usuario logueado para enviar correos
     */
    public function updateUser($user, $companyName=null, $serviceName=null, $checkLoggedUserPermissions = false){
        $this->em->flush();
        if(!$user->getAccountValidated()){
            $this->baseKeysService->sendAccountValidationEmail(new SendAccountValidationEmailOptions([
                'user' => $user, 'companyName' => $companyName, 'serviceName' => $serviceName, 'checkLoggedUserPermissions' => $checkLoggedUserPermissions
            ]));
        }
        return true;
    }
    /*
     * Get form to update password user
     */
    public function getUpdatePasswordForm($action = ""){
        return $this->formFactory->createBuilder()
                ->add('password', RepeatedType::class, array(
                    'required'=>false,
                    'type' => PasswordType::class,
                    'invalid_message' => 'Las contraseñas deben coincidir.',
                    'first_options'  => array('label' => 'Contraseña'),
                    'second_options' => array('label' => 'Repetir contraseña')
                ))
                ->add(
                        'cambiar', 
                        SubmitType::class, 
                        array(
                            'label'=>'Cambiar',
                            'attr'=>array(
                                'class'=>'btn btn-info'
                                )
                            )
                        )
                ->setAction($action)
                ->getForm();    
    }
    /*
     * Update password from valid form 
     */
    public function updatePassword($user, $form){
        if($form->isValid()){
            $user->setSalt(md5(rand(1,1000).$user->getEmail()));
            $encoder = $this->encoderFactory->getEncoder($user);
            $user->setPassword($encoder->encodePassword($form->get('password')->getData(),$user->getSalt()));
            $this->em->flush();
            return true;
        }else{
            return false;
        }
    }
    /*
     * Toggle state 
     */
    public function toggleUserState($user){
        if($user->getState()==1){
            $user->setState(0);
        }else{
            $user->setState(1);
        }
        $this->em->flush();
        return $user->getState();
    }
    
    public function removeUser($user, UserManagementDeleteOptions $options){
        
        if($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')||($options->force)){
            $this->doRemoveUser($user);
            return true;
        }
        
        $current_user = $this->tokenStorage->getToken()->getUser();
        
        if($user->getId() == $current_user->getId()){
            $this->errors[] = 'No puedes eliminar tu propio usuario';
            return false;
        }

        if($this->options['on_delete_check_admin_users']){
            $repo = $this->em->getRepository('dsarhoya\BaseBundle\Entity\BaseUser');
            $admin_user = $repo->otherAdminUser($user, $this->userClass);

            if(is_null($admin_user)){
                $this->errors[] = 'El sistema no puede quedar sin usuarios administradores';
                return false;
            }
        }
        
        $this->doRemoveUser($user);
        return true;
    }
    
    private function doRemoveUser($user){
        $this->em->remove($user);
        $this->em->flush();
    }
    
    public function getErrorsAsString(){
        return implode(', ', $this->errors);
    }
}
