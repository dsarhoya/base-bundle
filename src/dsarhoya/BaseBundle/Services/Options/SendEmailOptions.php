<?php

namespace dsarhoya\BaseBundle\Services\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;
use dsarhoya\BaseBundle\Options\BaseOptions;

/**
 * Description of SendEmailOptions
 *
 * @author matias
 */
class SendEmailOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'from' => null,
            'cc' => null,
            'bcc' => null,
            'fromName' => null,
        ));
        
        $resolver->setRequired('to')->setAllowedTypes('to', array('array'));
        $resolver->setAllowedTypes('from', array('null', 'string'));
        $resolver->setAllowedTypes('fromName', array('null', 'string'));
        $resolver->setRequired('subject')->setAllowedTypes('subject', array('string'));
        $resolver->setRequired('body')->setAllowedTypes('body', array('string'));
        $resolver->setAllowedTypes('cc', array('null', 'array'));
        $resolver->setAllowedTypes('bcc', array('null', 'array'));
    }
    
    public function to() {
        return $this->to;
    }
    
    public function toName() {
        return $this->toName;
    }
    
    public function from() {
        return $this->from;
    }
    
    public function fromName() {
        return $this->fromName;
    }
    
    public function subject() {
        return $this->subject;
    }
    
    public function body() {
        return $this->body;
    }
    
    public function cc() {
        return $this->cc;
    }
    
    public function bcc() {
        return $this->bcc;
    }
}
