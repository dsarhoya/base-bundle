<?php

namespace dsarhoya\BaseBundle\Services\Options\BaseKeys;

use Symfony\Component\OptionsResolver\OptionsResolver;
use dsarhoya\BaseBundle\Options\BaseOptions;
use dsarhoya\BaseBundle\Entity\BaseUser;

/**
 * Description of SendAccountValidationEmailOptions
 *
 * @author matias
 */
class CreateKeyOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'privateKeySeed' => 'qwerty',
            'lifespan' => 2,
        ));
        
        $resolver->setRequired('user')->setAllowedTypes('user', [BaseUser::class]);
        $resolver->setRequired('end')->setAllowedTypes('end', ['string']);
        $resolver->setAllowedTypes('privateKeySeed', ['string']);
        $resolver->setAllowedTypes('lifespan', ['int']);
    }
}
