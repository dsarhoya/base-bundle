<?php

namespace dsarhoya\BaseBundle\Services\Options\BaseKeys;

use Symfony\Component\OptionsResolver\OptionsResolver;
use dsarhoya\BaseBundle\Options\BaseOptions;
use dsarhoya\BaseBundle\Entity\BaseUser;

/**
 * Description of SendAccountValidationEmailOptions
 *
 * @author matias
 */
class SendAccountValidationEmailOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'checkLoggedUserPermissions' => false,
            'companyName' => null,
            'serviceName' => null,
        ));
        
        $resolver->setRequired('user')->setAllowedTypes('user', [BaseUser::class]);
        $resolver->setAllowedTypes('checkLoggedUserPermissions', ['bool']);
        $resolver->setAllowedTypes('companyName', ['null', 'string']);
        $resolver->setAllowedTypes('serviceName', ['null', 'string']);
    }
}
