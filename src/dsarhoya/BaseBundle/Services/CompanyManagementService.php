<?php

namespace dsarhoya\BaseBundle\Services;

use dsarhoya\BaseBundle\Entity\BaseCompany;
use dsarhoya\BaseBundle\Form\Type\CompanyType;

/**
 * Servicio para crear y editar empresas en los proyectos sin tener que repetir la lógica
 *
 * @author kito
 */
class CompanyManagementService {
    
    private $em;
    private $encoderFactory;
    private $formFactory;
    private $tokenStorage;
    private $authorizationChecker;
    private $companyClass;
    
    private $errors;
    
    public function __construct($entityManager, $encoderFactory, $formFactory, $tokenStorage, $authorizationChecker, $classes){
        $this->em               = $entityManager;
        $this->encoderFactory   = $encoderFactory;
        $this->formFactory      = $formFactory;
        $this->tokenStorage     = $tokenStorage;
        $this->authorizationChecker  = $authorizationChecker;
        $this->companyClass     = $classes['company']['class'];
        
        $this->errors           = array();
    }
    
    /*
     * Get form to create or update company
     */
    public function getForm($company,$baseFieldsToInclude, $admin = false, $formType = null, $options = array()){
        if(is_null($formType)){
            $formType = CompanyType::class;
        }
        
        return $this->formFactory->create(
            $formType,
            $company, 
            array_merge(
                    array("baseFieldsToInclude"=>$baseFieldsToInclude),
                    $options
                )
            );
    }
    
    /*
     * Persist company in the data base
     */
    public function persistCompany($company){
        
        if(is_null($company->getState())){
            $company->setState(BaseCompany::ESTADO_BLOQUEADA);
        }
        $this->em->persist($company);
        $this->em->flush();
        
        return true;
    }
    
    /*
     * Update company in the data base 
     * for the moment, it doesnt do anything.
     */
    public function updateCompany($company){
        $this->em->flush();
        return true;
    }
    
    /*
     * Toggle state 
     */
    public function toggleCompanyState($company){
        if ( $company->getState() == BaseCompany::ESTADO_ACTIVA ){
            $company->setState(BaseCompany::ESTADO_BLOQUEADA);
        }else{
            $company->setState(BaseCompany::ESTADO_ACTIVA);
        }
        $this->em->flush();
        return $company->getState();
    }
    
    public function removeCompany($company){
        
        if($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')){
            $this->doRemoveCompany($company);
            return true;
        }
        
        $current_user = $this->tokenStorage->getToken()->getUser();
        if($company->getId() == $current_user->getCompany()->getId()){
            $this->errors[] = 'No puedes eliminar tu propio usuario';
            return false;
        }
        $this->doRemoveCompany($company);
        return true;
    }
    
    private function doRemoveCompany($company){
        $this->em->remove($company);
        $this->em->flush();
    }
    
    public function getErrorsAsString(){
        return implode(', ', $this->errors);
    }
}
