<?php

namespace dsarhoya\BaseBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\BaseBundle\Entity\BaseCompany;
use dsarhoya\BaseBundle\Services\CompanyManagementService;
use dsarhoya\BaseBundle\Services\ParametersService;
use dsarhoya\BaseBundle\Services\UserManagementService;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserCommand extends BaseCommand
{
    private $parametersService;
    private $companyManagementService;
    private $userManagementService;
    private $tokenStorageInterface;
    private $validator;

    public function __construct(
        EntityManagerInterface $em, 
        ParametersService $parametersService,
        CompanyManagementService $companyManagementService,
        UserManagementService $userManagementService,
        TokenStorageInterface $tokenStorageInterface,
        ValidatorInterface $validator
    )
    {
        $this->parametersService = $parametersService;
        $this->companyManagementService = $companyManagementService;
        $this->userManagementService = $userManagementService;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->validator = $validator;
        parent::__construct($em);
    }

    protected function configure()
    {
        $this
            ->setName('dsarhoya:create-user')
            ->setDescription('Creates admin user')
            ->addArgument('name', InputArgument::REQUIRED, 'Your name')
            ->addArgument('email', InputArgument::REQUIRED, 'Your email')
            ->addArgument('company-name', InputArgument::REQUIRED, 'Company name')
            ->addOption('create-company', 'c', InputOption::VALUE_NONE, 'Create company if not existent?')
            ->addOption('no-validate', 'nv', InputOption::VALUE_NONE, 'Skip validation')
            ->addOption('account-validated', 'av', InputOption::VALUE_NONE, 'Force account validated')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $classes = $this->parametersService->classes;

        $output->writeln(sprintf('Create user %s, email %s', $input->getArgument('name'), $input->getArgument('email')));

        $company = $this->repo($classes['company']['class'])->findOneByName($input->getArgument('company-name'));

        if (null === $company && true === $input->getOption('create-company')) {
            $company = new $classes['company']['class']();
            $company->setName($input->getArgument('company-name'));
            $company->setRut('1-9');
            $company->setState(BaseCompany::ESTADO_ACTIVA);
            $this->companyManagementService->persistCompany($company);
        } elseif (null === $company) {
            throw new \Exception("Company {$input->getArgument('company-name')} not found.");
        }

        $user = new $classes['user']['class']();
        $user->setEmail($input->getArgument('email'));
        $user->setName($input->getArgument('name'));
        $user->setEmailSender(1);
        $user->setCompany($company);

        if (true === $input->getOption('account-validated')) {
            $user->setAccountValidated(true);
        }

        if (false === $input->getOption('no-validate')) {
            $errors = $this->validator->validate($user);
            if (count($errors) > 0) {
                $output->writeln(sprintf('There were errors creating the user: %s', (string) $errors));

                return;
            }
        }

        $token = new \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken(
            $user,
            null,
            'main',
            ['ROLE_SUPER_ADMIN']);
        $this->tokenStorageInterface->setToken($token);

        $this->userManagementService->persistUser($user);

        $output->writeln('User was created successfully.');
    }
}
