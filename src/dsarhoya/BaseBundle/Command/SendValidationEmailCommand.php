<?php

namespace dsarhoya\BaseBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendValidationEmailCommand extends BaseCommand {
    protected function configure()
    {
        $this
            ->setName('dsarhoya:send-validation-email')
            ->setDescription('Send a validation email for a not activated account')
            ->addArgument(
                'email',
                 \Symfony\Component\Console\Input\InputArgument::REQUIRED,
                'Email address'
            )
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output){
        
        $classes = $this->getContainer()->getParameter('dsarhoya_base.classes');

        $user = $this->repo($classes['user']['class'])->findOneBy(array('email'=>$input->getArgument('email')));
        if(is_null($user)){
            $output->writeln(sprintf('User with email %s not found', $input->getArgument('email')));
            return;
        }
        
        if($user->getAccountValidated() === $classes['user']['class']::ACCOUNT_VALIDATED){
            $output->writeln(sprintf('User with email %s has already validated the account', $input->getArgument('email')));
            return;
        }
        
        $userService = $this->get('dsarhoya.base.usermanagement');
        
        $token = new \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken(
            $user,
            null,
            'main',
            ['ROLE_SUPER_ADMIN']);
        $this->get('security.context')->setToken($token);
        
        $userService->updateUser($user);
        
        $output->writeln('Email sent.');
    }
}
