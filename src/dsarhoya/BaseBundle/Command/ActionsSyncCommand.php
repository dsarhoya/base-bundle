<?php
namespace dsarhoya\BaseBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\BaseBundle\Services\ParametersService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ActionsSyncCommand extends BaseCommand 
{

    private $parametersService;

    public function __construct(
        EntityManagerInterface $em, 
        ParametersService $parametersService
    )
    {
        $this->parametersService = $parametersService;
        parent::__construct($em);
    }

    protected function configure()
    {
        $this
            ->setName('dsarhoya:actions:sync')
            ->setDescription('Syncroniza las acciones definidas en la clase ACL con la base de datos.')
            ->addOption('detalle', null, InputOption::VALUE_NONE, 'Con este parametro la tarea dice que hace con cada accion.')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // try{
        //     $log = $this->getContainer()->get('dsarhoya.logs')->newCommandLog('actions_sync');   
        // }catch(\Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException $e){
            
        // }
        
        $params = $this->parametersService->acl;
        $classes = $this->parametersService->classes;
        
        if(!isset($params['class']))
            throw new \Exception('Debes especificar la clase contenedora de los permisos en config -> parameters');
        
        $customAcl = new $params['class'];
        
        if(!is_subclass_of($customAcl, 'dsarhoya\BaseBundle\Entity\CustomAcl'))
            throw new \Exception('La clase debe extender de CustomAcl');
        
        $user_defined_actions = $customAcl::values();
        
        $rep = $this->em->getRepository($classes['action']['class']);
        $actions = $rep->findAll();
        $output->writeln('Inicio del proceso...');
        
        //Primero voy a borrar las que ya no están definidas.
        foreach ($actions as $action) {
            if(!array_key_exists($action->getId(), $user_defined_actions)){
                $output->writeln('La accion '.$action->getId().' ya no existe! Se elimina.');
                $this->em->remove($action);
            }
        }

        //Después, miro las del usuario y guardo las que no existían
        foreach ($user_defined_actions as $action_id => $description) {
            $action = $rep->find($action_id);
            if(!$action){
                $output->writeln('La accion '.$action_id.', con descripción "'.$description.'" es nueva y se guardara');
                // if(isset($log)) $log->addComment('La accion '.$action_id.', con descripción "'.$description.'" es nueva y se guardara');
                $new_action = new $classes['action']['class'];
                $new_action->setId($action_id);
                $new_action->setDescription($customAcl::explanationFor($action_id));
                $new_action->setDisplay($description);
                $this->em->persist($new_action);
            }else{
                /* @var $action Action */
                $action->setDescription($customAcl::explanationFor($action_id));
                $action->setDisplay($description);
                ($input->getOption('detalle')) ? $output->writeln('La accion '.$action_id.' ya estaba guardada.'):null;
                // ($input->getOption('detalle') && isset($log)) ? $log->addComment('La accion '.$action_id.' ya estaba guardada.'):null;
            }
        }
        // if(isset($log)) $log->setDescription('Terminó el proceso de sincronización.');
        // if(isset($log)) $log->setStatus(\dsarhoya\DSYLogsBundle\Entity\DSYCommandLog::DSY_COMMAND_LOG_STATUS_FINISHED);
        // if(isset($log)) $em->persist($log);
        $this->em->flush();
        $output->writeln('Fin');
    }

}
