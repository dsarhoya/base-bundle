<?php

namespace dsarhoya\BaseBundle\Command;

/**
 * Description of SetupCommand
 *
 * @author matias
 */
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

/**
 * @todo falta sacar el warning.
 */
class SetupCommand extends ContainerAwareCommand
{
    
    CONST HELPER_TYPE_NONE = 0;
    CONST HELPER_TYPE_QUESTION = 1;
    CONST HELPER_TYPE_DIALOG = 2;
    
    private $helper_type = 0;
    private $helper;
    
    private $input;
    private $output;
    
    private $environment;
    
    private $force = false;


    protected function configure()
    {
        $this
            ->setName('dsarhoya:setup')
            ->setDescription('General setup')
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Forzar el proceso?'
            );
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input    = $input;
        $this->output   = $output;
        $this->force    = $input->getOption('force');
        //First check for the helper
        foreach ($this->getHelperSet() as $helper) {
            if($helper instanceof \Symfony\Component\Console\Helper\DialogHelper && $this->helper_type == self::HELPER_TYPE_NONE){
                $this->helper = $helper;
                $this->helper_type = self::HELPER_TYPE_DIALOG;
            }elseif ($helper instanceof \Symfony\Component\Console\Helper\QuestionHelper) {
                $this->helper = $helper;
                $this->helper_type = self::HELPER_TYPE_QUESTION;
            }
        }
        
        $kernel = $this->getContainer()->get('kernel');
        $this->environment = $kernel->getEnvironment();
        $output->writeln(sprintf('<comment>Proceso para ambiente %s</comment>', $this->environment));
        if(!$this->askConfirmation('<question>Esta eso correcto? (yes/no) </question>')){
            $output->writeln(sprintf('<comment>Para correr otro ambiente, usar --env=env o --env=prod</comment>', $this->environment));
            return;
        }
        
        $schema_update_command = $this->getApplication()->find('doctrine:schema:update');
        $arguments = array(
            'command' => 'doctrine:schema:update',
            '--dump-sql' => true
        );

        $input = new ArrayInput($arguments);
        $this->output->writeln('<info>doctrine:schema:update dice:</info>');
        $schema_update_command->run($input, $this->output);
        
        if($this->force||$this->askConfirmation('<question>Actualizar la base de datos? (yes/no) </question>')){
            $force_arguments = array(
                'command' => 'doctrine:schema:update',
                '--force' => true
            );

            $input = new ArrayInput($force_arguments);
            $schema_update_command->run($input, $this->output);
        }
        
        if($this->force||$this->askConfirmation('<question>Install assets? (yes/no) </question>')){
            $install_assets_command = $this->getApplication()->find('assets:install');
            $install_assets_arguments = array(
                'command' => 'assets:install'
            );

            $input = new ArrayInput($install_assets_arguments);
            $install_assets_command->run($input, $this->output);
        }
        
        if($this->getApplication()->has('assets-version:increment') && ($this->force||$this->askConfirmation('<question>Increment version? ('.$this->environment.')? (yes/no) </question>'))){
            
            $assets_version_command = $this->getApplication()->find('assets-version:increment');
            $assets_version_arguments = array(
                'command'   => 'assets-version:increment',
//                '--env'     => $this->environment
            );

            $input = new ArrayInput($assets_version_arguments);
            $assets_version_command->run($input, $this->output);
            
            $cache_clear_command = $this->getApplication()->find('cache:clear');
            $cache_clear_arguments = array(
                'command'   => 'cache:clear'
            );

            $cache_clear_input = new ArrayInput($cache_clear_arguments);
            $cache_clear_command->run($cache_clear_input, $this->output);
        }
        
        if($this->getApplication()->has('assetic:dump') && ($this->force||$this->askConfirmation('<question>Dump assets ('.$this->environment.')? (yes/no) </question>'))){
            $dump_assets_command = $this->getApplication()->find('assetic:dump');
            $dump_assets_arguments = array(
                'command'   => 'assetic:dump',
            );

            $input = new ArrayInput($dump_assets_arguments);
            $dump_assets_command->run($input, $this->output);
        }
        
        if($this->force||$this->askConfirmation('<question>Clear cache ('.$kernel->getEnvironment().')? (yes/no) </question>')){
            $cache_clear_command = $this->getApplication()->find('cache:clear');
            $cache_clear_arguments = array(
                'command'   => 'cache:clear'
            );

            $cache_clear_input = new ArrayInput($cache_clear_arguments);
            $cache_clear_command->run($cache_clear_input, $this->output);
        }
    }
    
    private function askConfirmation($question, $default = false){
        if($this->helper_type == self::HELPER_TYPE_DIALOG){
            return $this->helper->askConfirmation(
                $this->output,
                $question,
                $default
            );
        }elseif ($this->helper_type == self::HELPER_TYPE_QUESTION) {
            $question = new \Symfony\Component\Console\Question\ConfirmationQuestion($question, $default);
            return $this->helper->ask($this->input, $this->output, $question);
        }
    }
    
    private function askChoice($question, array $choices, $default = 0){
        if($this->helper_type == self::HELPER_TYPE_DIALOG){
            throw new \Exception('No se ha implementado el tipo Choice para el helper tipo Dialog.');
        }elseif ($this->helper_type == self::HELPER_TYPE_QUESTION) {
            $question = new \Symfony\Component\Console\Question\ChoiceQuestion($question, $choices, $default);
            $question->setErrorMessage('%s is invalid.');
            return $this->helper->ask($this->input, $this->output, $question);
        }
    }
}