<?php

namespace dsarhoya\BaseBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;

abstract class BaseCommand extends Command
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    /**
     * @param type $class
     * @param type $bundle
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function repo($class = null)
    {
        if (!is_string($class)) {
            $class = $this->class;
        }

        return $this->em->getRepository("$class");
    }

    public function get($service)
    {
        throw new \Exception('No usar el contenedor directamente, inyectar la dependencia');
    }
}
