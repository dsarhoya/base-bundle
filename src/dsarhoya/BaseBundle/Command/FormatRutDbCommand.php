<?php
namespace dsarhoya\BaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use dsarhoya\BaseBundle\Form\Type\Rut\RutTransformer;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Description of FormatRUTCommand
 *
 * @author snake77se
 */
class FormatRutDbCommand extends ContainerAwareCommand
{
    /**
     * 
     */
    protected function configure()
    {
        $this
            ->setName('dsarhoya:BaseBundle:Company:formatRutDb')
            ->setDescription('Command that allows formatting existing RUT in the database.')
            ->addOption('add_dots', 'd', InputOption::VALUE_NONE, 'If set, fotmat with dots.')
        ;
    }
    /**
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $repo = $em->getRepository('dsarhoyaSCBundle:Company');
        $companies = $repo->findAll();
        $progress = new ProgressBar($output, count($companies));
        $rutFormater = new RutTransformer($input->getOption('add_dots')?true:false);
        
        $output->writeln('Inicio del proceso...');
        foreach ($companies as $c){
            $dirtyRUT = $c->getRut();
            try{
                $c->setRut($rutFormater->reverseTransform($dirtyRUT));
            }
            catch(TransformationFailedException $e){
                $output->writeln('Fallo en el ID '.$c->getId().", con el RUT: ".$dirtyRUT);
                continue;
            }
            $em->persist($c);
            $em->flush();
            $progress->advance();
        }
        $progress->finish();
        $output->writeln('');
        $output->writeln('Fin');
    }
}
