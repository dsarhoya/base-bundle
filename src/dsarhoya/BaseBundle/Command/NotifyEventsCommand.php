<?php
namespace dsarhoya\BaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use dsarhoya\BaseBundle\Entity\BaseNotifiableEvent;

class NotifyEventsCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('dsarhoya:events:notify')
            ->setDescription('Envía las notificaciones que correspondan.')
            ->addOption('detalle', null, InputOption::VALUE_NONE, 'Con este parametro la tarea dice que hace con cada accion.')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Comienzo de notificaciones');
        
        try{
            $log = $this->getContainer()->get('dsarhoya.logs')->newCommandLog('events_notify');   
        }catch(\Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException $e){
            
        }
        
        $classes = $this->getContainer()->getParameter('dsarhoya_base.classes');
        
        $doctrine = $this->getContainer()->get('doctrine');
        $templating = $this->getContainer()->get('templating');
        //$templting->render()...;
        
        $em = $doctrine->getManager();
        //$rep = $doctrine->getRepository("dsarhoyaBaseBundle:BaseNotificationFolderInterface");
        $rep = $doctrine->getRepository($classes['notificationFolder']['class']);
        $events = $rep->getPendingEvents();
        
        $input->getOption('detalle') ? $output->writeln('Conteo de eventos: '.count($events).'.'):null;
        foreach($events as $event){
            $input->getOption('detalle') ? $output->writeln('idEvent: '.$event->getId().'. Is notifiable? '.($event->isNotifiable(new \DateTime('now')) ? 'yes' : 'no')):null;
            if($event->isNotifiable(new \DateTime('now'))){
                foreach($event->getMessages() as $message){
                    //$output->writeln('Event: '.$event->getDate()->format('d/m/Y'));
                    
                    $mailMessage = \Swift_Message::newInstance()
                    ->setSubject($message->getSubject())
                    ->setFrom('contacto@dsarhoya.cl')
                    ->setTo($message->getAddressee())
                    ->setCc($message->getCC())
                    ->setCharset('UTF-8')
                    ->setContentType('text/html')       
                    ->setBody($templating->render($message->getBodyTemplate(),$message->getBodyParameters()));

                    if($this->getContainer()->get('mailer')->send($mailMessage)>0){
                        $input->getOption('detalle') ? $output->writeln('idEvent: '.$event->getId().'. mensaje enviado a: '.$message->getAddressee()):null;
                        if(isset($log)) $log->addComment('Mensaje para:'. $message->getAddressee().' enviado.');
                        $event->setStatus(BaseNotifiableEvent::STATUS_SENT);
                    }else{
                        $input->getOption('detalle') ? $output->writeln('idEvent: '.$event->getId().'. mensaje no pudo ser enviado a: '.$message->getAddressee()):null;
                        if(isset($log)) $log->addComment('ERROR: Mensaje para: '. $message->getAddressee().' no pudo ser enviado.');
                        $event->setStatus(BaseNotifiableEvent::STATUS_ERROR);
                    }
                }
            }
        }
        
        if(isset($log)) {
            $log->setStatus(\dsarhoya\DSYLogsBundle\Entity\DSYCommandLog::DSY_COMMAND_LOG_STATUS_FINISHED);
            $log->setDescription('Terminó el proceso de notificaciones.');
            $em->persist($log);
        }
         
        $em->flush();
        
        $output->writeln('Fin');
    }

}
