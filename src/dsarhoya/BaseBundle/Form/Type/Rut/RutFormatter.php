<?php

namespace dsarhoya\BaseBundle\Form\Type\Rut;

/**
 * Description of RutFormatter
 *
 * @author mati, snake77se
 */
class RutFormatter implements RutFormatterInterface
{
    private $addDots;
    
    public function __construct($addDots = false) {
        $this->addDots = $addDots;
    }
    
    public function format($rut) {
        
        if(null === $rut OR strlen($rut)==0){ 
            return $rut;
        }
        if(count(str_split($rut)) <= 1){
            throw new \Symfony\Component\Form\Exception\TransformationFailedException;
        }
        $rutArr = $this->splitRut($rut);
        return ($this->addDots)?number_format($rutArr["rut"],0,'','.')."-".$rutArr["dv"]:$rutArr["rut"]."-".$rutArr["dv"];
    }
    
    /**
     * Valida y formatea RUT en distintos formatos.
     * Esta clase considera que los RUT son manejados como sólo una cadena de texto, es decir,
     * incluye SIEMPRE el digito verificador.
     *
     * @author Esteban Martini Muñoz
     * @version 1.0
     * https://emartini.wordpress.com/2009/11/14/clase-para-validar-rut-en-php/
     */
    
    /**
     * remueve caracteres que actuan como separadores dentro del
     * RUT (espacios, puntos, guiones,etc)
     *
     * @param string $r RUT
     * @return string RUT sin caracteres separadores.
     */
    public function formatoUser($r){
        return preg_replace('/(\.)|(\-)|[ ]|[\,]|[\']/','',$r);
    }
    
    /**
     * Crea un arreglo asociativo separando el RUT y el dígito verificador.
     * @param string $r
     * @return string[]
     */
    public function splitRut($r)
    {
        //sacar puntos, guiones, espacios, etc:
        $rutClean = $this->formatoUser($r);
        
        $dv = strtoupper(substr($rutClean,strlen($rutClean)-1));
        $rutNum = substr($rutClean,0,strlen($rutClean)-1);
        
        return array("rut"=>$rutNum, "dv"=>$dv);
    }
}
