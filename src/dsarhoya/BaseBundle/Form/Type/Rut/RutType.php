<?php

namespace dsarhoya\BaseBundle\Form\Type\Rut;

use dsarhoya\BaseBundle\Form\Type\Rut\RutTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Description of RutType
 *
 * @author mati
 */
class RutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new RutTransformer($options['add_dots']);
        $builder->addModelTransformer($transformer);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'invalid_message' => 'Formato inválido',
            'add_dots' => false
        ));
    }
    
    public function getParent()
    {
        return TextType::class;
    }
    
    public function getName()
    {
        return 'auth_rut';
    }
    
    public function getBlockPrefix() {
        return 'auth_rut';
    }
}
