<?php

namespace dsarhoya\BaseBundle\Form\Type\Rut;

/**
 *
 * @author mati
 */
interface RutFormatterInterface {
    public function format($rut);
}
