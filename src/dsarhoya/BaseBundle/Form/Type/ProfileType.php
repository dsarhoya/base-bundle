<?php

namespace dsarhoya\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProfileType extends AbstractType
{
    protected $profile_class;
    protected $company_class;
    
    public function __construct($classes) {
        $this->profile_class = $classes['profile']['class'];
        $this->company_class = $classes['company']['class'];
    }
    
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null, ["label"=>"Nombre"]);
        if($options['admin']){
            
            $builder
                ->add('isAdmin', null, array(
                    'label'=>'Is Admin profile?',
                    'required'=>false
                ));
        }
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Guardar',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->profile_class,
            'admin' => false,
            'user'  => null
        ));
    }
}
