<?php

namespace dsarhoya\BaseBundle\Form\Type\CustomType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\SubmitButtonTypeInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

/**
 * Description of SubmitAndCancelType
 *
 * @author mati
 */
class SubmitAndCancelType extends AbstractType implements SubmitButtonTypeInterface
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cancel_url' => null,
            'cancel_class' => 'btn btn-danger',
            'cancel_label' => 'Cancel',
        ));
    }
    
    public function getBlockPrefix() {
        return 'submitandcancel';
    }

    public function getParent()
    {
        return SubmitType::class;
    }
    
    public function getName() {
        return 'submit_and_cancel';
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['cancel_url'] = $options['cancel_url'];
        $view->vars['cancel_class'] = $options['cancel_class'];
        $view->vars['cancel_label'] = $options['cancel_label'];
    }
    
}
