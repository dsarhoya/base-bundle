<?php

namespace dsarhoya\BaseBundle\Form\Type\Options;

use dsarhoya\BaseBundle\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaintenanceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE, null, [
                'label' => 'Título',
                'required' => true,
                'disabled' => $options['in_maintenance'],
            ])
            ->add(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE, null, [
                'label' => 'Mensaje',
                'required' => true,
                'disabled' => $options['in_maintenance'],
            ])
            ->add('create', SubmitType::class, [
                'label' => 'Crear mantenimiento',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
                'disabled' => $options['in_maintenance'],
            ])
            ->add('remove', SubmitType::class, [
                'label' => 'Eliminar mantenimiento',
                'attr' => [
                    'class' => 'btn btn-success',
                ],
                'disabled' => !$options['in_maintenance'],
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'in_maintenance' => false
        ));
    }
}
