<?php

namespace dsarhoya\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{    
    protected $user_class;
    protected $company_class;
    protected $profile_class;
    
    public function __construct($classes) {
        $this->user_class = $classes['user']['class'];
        $this->company_class = $classes['company']['class'];
        $this->profile_class = $classes['profile']['class'];
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        if(!isset($options['company'])) throw new \Exception('Para crear un usuario hay que definir la empresa primero');
        /*This field must be in every form*/
        $builder->add('name',null,array('label'=>'Nombre'));
        /*This field must be in every form*/
        $builder->add('email', EmailType::class, array('label'=>'Email'));
        
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("state", $options["baseFieldsToInclude"])){
            $builder->add('state', ChoiceType::class,array(
                'label'     => 'Estado',
                'multiple'  => false,
                'expanded'  => false,
                'choices'   => array('Activo' => '1','Deshabilitado' => '0'),
                ));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("accessCode", $options["baseFieldsToInclude"])){
            $builder->add('accessCode', ChoiceType::class, array(
                'label'     => 'Requiere código de acceso',
                'multiple'  => false,
                'expanded'  => false,
                'choices'   => array('Sí' => '1','No' => '0'),
                ));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("emailSender", $options["baseFieldsToInclude"])){
            $builder->add('emailSender', ChoiceType::class, array(
                'label'     => 'Envío de emails',
                'multiple'  => false,
                'expanded'  => false,
                'choices'   => array('Todo el sistema' => '1', 'Solo en su empresa' => '0'),
                ));
        }
        if($options['admin']){
            $builder->add('accountValidated', ChoiceType::class, array(
                'label'     => 'Cuenta validada',
                'multiple'  => false,
                'expanded'  => false,
                'choices'   => array('Sí' => '1','No' => '0'),
                ));
        }
        
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("profile", $options["baseFieldsToInclude"])){
           $builder->add('profile', EntityType::class, array(
                                    'required'      => false,
                                    'class'         => $this->profile_class,
                                    'choice_label'  => 'name',
                                    'label'         => 'Perfil',
                                    'multiple'      => FALSE,
                                    'expanded'      => FALSE,
                                    'placeholder'   => 'Escoja una opción...',    
                                    'choices'       => $options['company']->getProfiles()
                                    ));
        }
        
        foreach ($options['additionalFieldsToInclude'] as $field) {
           $builder->add($field['name'], isset($field['type']) ? $field['type'] : null, isset($field['options']) ? $field['options'] : array());
        }
        
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Guardar',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ));
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->user_class,
//            'cascade_validation' => true, //Deprecated
            'admin'=>false,
            'company'=> null,
            "baseFieldsToInclude"=>array(),
            "additionalFieldsToInclude"=>array()
        ));
    }

    public function getDefaultOptions(array $options)
    {
    return array(
        'data_class' => $this->user_class
        );
    }
}
