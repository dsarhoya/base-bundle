<?php

namespace dsarhoya\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use dsarhoya\BaseBundle\Form\Type\Rut\RutType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CompanyType extends AbstractType
{    
    protected $company_class;
    
    public function __construct($classes) {
        $this->company_class = $classes['company']['class'];
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*name and rut are required in every form*/
        $builder->add('name',null,array('label'=>'Razón social'));
        if(!$options["format_rut"]){
            $builder->add('rut',null,array('label'=>'RUT'));
        }
        else{
            $builder->add('rut',RutType::class, array('label'=>'RUT', "add_dots"=>$options["add_dots"]));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("address", $options["baseFieldsToInclude"])){
            $builder->add('address',null,array('label'=>'Dirección'));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("locality", $options["baseFieldsToInclude"])){
            $builder->add('locality',null,array('label'=>'Comuna'));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("notes", $options["baseFieldsToInclude"])){
            $builder->add('notes',null,array('label'=>'Notas'));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("maxConcurrentSessions", $options["baseFieldsToInclude"])){
            $builder->add('maxConcurrentSessions',null,array('label'=>'Máximo sesiones concurrentes'));
        }
        /*include each field if required by service*/
        if(in_array("all", $options["baseFieldsToInclude"]) ||  in_array("state", $options["baseFieldsToInclude"])){
            $builder->add('state', ChoiceType::class, array(
                'label'=>'Estado',
                'multiple'=>false,
                'expanded'=>false,
                'choices'   => array(
                    'Activa' => '1',
                    'Bloqueada' => '0'
                )
                ));
        }
        
        foreach ($options['additionalFieldsToInclude'] as $field) {
           $builder->add($field['name'], isset($field['type']) ? $field['type'] : null, isset($field['options']) ? $field['options'] : array());
        }
        
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Guardar',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ));
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->company_class,
//            'cascade_validation' => true, //Deprecado, ahora hay que usar el constraint "Valid".
            "baseFieldsToInclude"=>array(),
            "additionalFieldsToInclude"=>array(),
            "format_rut"=>false,
            "add_dots"=>false,
        ));
    }
    
    public function getDefaultOptions(array $options)
    {
    return array(
        'data_class' => $this->company_class
        );
    }
}