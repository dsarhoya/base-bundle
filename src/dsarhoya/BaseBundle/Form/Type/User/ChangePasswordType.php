<?php

namespace dsarhoya\BaseBundle\Form\Type\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ChangePasswordType extends AbstractType
{    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('pass', PasswordType::class,array(
            'required' => false,
            'label'=>'Contraseña'
        ));
        $builder->add('repeatedPass', PasswordType::class,array(
            'required' => false,
            'label'=>'Repetir contraseña'
        ));
        
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }
}