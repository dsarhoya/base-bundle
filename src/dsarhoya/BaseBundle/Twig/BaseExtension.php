<?php

namespace dsarhoya\BaseBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class BaseExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('flash', [BaseRuntime::class, 'flash'], [
                'is_safe' => ['html'], 
                'needs_environment' => true
            ]),
            new TwigFunction('form_company', [BaseRuntime::class, 'companyForm'], [
                'is_safe' => ['html'], 
                'needs_environment' => true
            ]),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('routeForRole', [BaseRuntime::class, 'routeForRole']),
        ];
    }
}