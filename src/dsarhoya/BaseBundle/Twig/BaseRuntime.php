<?php

namespace dsarhoya\BaseBundle\Twig;

use dsarhoya\BaseBundle\Services\ParametersService;
use Twig\Extension\RuntimeExtensionInterface;
use Twig\Environment;

class BaseRuntime implements RuntimeExtensionInterface
{
    
    private $parametersService;

    public function __construct(ParametersService $parametersService)
    {
        $this->parametersService = $parametersService;
    }
    
    public function flash(Environment $env, $dismissible = false){
        
        return $env->render(
            '@dsarhoyaBase/Layout/flash.html.twig', ["dismissible"=>$dismissible]
        );
    }
    
    public function companyForm(Environment $env, $form){
        
        return $env->render(
            '@dsarhoyaBase/Layout/companyForm.html.twig',["form"=>$form]
        );
    }

    public function routeForRole($role)
    {
        return isset($this->parametersService->routes[$role]) ? $this->parametersService->routes[$role]['route'] : null;
    }
}
