<?php

namespace dsarhoya\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    
    CONST FLASH_TYPE_INFO = 'info';
    CONST FLASH_TYPE_SUCCESS = 'success';
    CONST FLASH_TYPE_ERROR = 'error';
    CONST FLASH_TYPE_WARNING = 'warning';
    
    protected function flash($message, $type = self::FLASH_TYPE_INFO){
        
        $this->get('session')->getFlashBag()->add(
            $type,
            $message
        );
        
    }
    
    /**
     * @param mixed $entity
     * @return Response
     */
    protected function returnSerializedEntity($entity){
        
        $serializer = $this->container->get('jms_serializer');
        $response = new Response($serializer->serialize($entity, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * @param array $array
     * @return Response
     */
    protected function returnJson($array){
        $response = new Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * @return \dsarhoya\DSYExtensionsBundle\Service\OrderAnnotationsService
     */
    protected function getServiceOrderAnnotations(){
        return $this->get('dsy.extensions.order_service');
    }
    
    /**
     * @return \dsarhoya\DSYFilesBundle\Services\DSYFilesService
     */
    protected function getServiceFiles(){
        return $this->get('dsarhoya.files');
    }
    
    /**
     * @return \dsarhoya\DSYLogsBundle\Services\DSYCommandLogsService
     */
    protected function getServiceCommandLogs(){
        return $this->get('dsarhoya.logs');
    }
    
    /**
     * @return \dsarhoya\DSYXLSBundle\Services\ExcelService
     */
    protected function getServiceExcel(){
        return $this->get('dsarhoya.xls');
    }
    
    /**
     * @return \dsarhoya\DSYApiKeyAuthenticatorBundle\Service\ApiKeyService
     */
    protected function getServiceApiKey(){
        return $this->get('dsy.api_key_service');
    }
    
    /**
     * @return \dsarhoya\BaseBundle\Services\UserManagementService
     */
    protected function getServiceUserManagementService(){
        return $this->get('dsarhoya.base.usermanagement');
    }
    
    /**
     * @return \dsarhoya\BaseBundle\Services\CompanyManagementService
     */
    protected function getServiceCompanyManagementService(){
        return $this->get('dsarhoya.base.companymanagement');
    }
    
    /**
     * @return \dsarhoya\BaseBundle\Services\ProfileManagementService
     */
    protected function getServiceProfileManagementService(){
        return $this->get('dsarhoya.base.profilemanagement');
    }
    
    /**
     * @return \dsarhoya\BaseBundle\Services\EmailService
     */
    protected function getServiceEmails(){
        return $this->get('dsarhoya.base.email_service');
    }
    
    /**
     * @return \dsarhoya\BaseBundle\Services\UserKeysService
     */
    protected function getServiceUserKeys(){
        return $this->get('dsarhoya.base.userKeysService');
    }
    
    /**
     * @return \dsarhoya\BaseBundle\Services\BaseKeysService
     */
    protected function getServiceBaseKeys(){
        return $this->get('dsarhoya\BaseBundle\Services\BaseKeysService');
    }
}
