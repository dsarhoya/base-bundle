<?php

namespace dsarhoya\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use dsarhoya\BaseBundle\Entity\Profile;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use dsarhoya\BaseBundle\Entity\BaseUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AuthenticationController extends AbstractController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $user;
    protected $container;
    private $classes;
    private $ACL;
    
    public function __construct($entityManager, $securityContext, $container) {
        $this->em           = $entityManager;
        $this->container    = $container;
        $this->classes = $this->container->getParameter('dsarhoya_base.classes');
        $this->ACL = $this->container->getParameter('dsarhoya_base.ACL');
        if (null === $token = $securityContext->getToken()) {
            $this->user =  null;
        }
        else{
            $this->user = $token->getUser(); 
        }
    }
    
    public function getProfiles(){
        $repoProfile = $this->em->getRepository($this->classes['profile']['class']);
        
        return $repoProfile->findBy(array('company'=>$this->user->getCompany()->getId()));
    }
    public function getProfile($idProfile){
        $repoProfile = $this->em->getRepository($this->classes['profile']['class']);
        
        return $repoProfile->find($idProfile);
    }
    public function createProfile($profileName){
        $repoCompany = $this->em->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($this->user->getCompany()->getId());
        $profile = new $this->classes['profile']['class'];
        $profile->setName($profileName);
        $profile->setCompany($company);
        $this->em->persist($profile);
        $this->em->flush();
        
        return $profile->getId();
    }
    public function updateProfile($idProfile, $profileName){
        $repoProfile = $this->em->getRepository($this->classes['profile']['class']);
        $profile=$repoProfile->find($idProfile);
        if($profile->getCompany()->getId() == $this->user->getCompany()->getId() && $profileName && $profileName != ""){
            $profile->setName($profileName);
            $this->em->flush();
            return true;
        }else{
            return false;
        }
    }
    public function deleteProfile($idProfile){
        $repoProfile = $this->em->getRepository($this->classes['profile']['class']);
        $profile=$repoProfile->find($idProfile);
        if($profile->getCompany()->getId() == $this->user->getCompany()->getId()){
            $this->em->remove($profile);
            $this->em->flush();
            return true;
        }else{
            return false;
        }
    }
    public function getActionTitles($actions = null){
        
        if(!isset($this->ACL['class']))
            throw new \Exception('Debes especificar la clas contenedora de los permisos');
        $customAcl = new $this->ACL['class'];
        
        if ( is_null($actions) ){
            return $customAcl::titles();
        }
        $titles = [];
        foreach( $actions as $action ){
            $title = $customAcl::titleFor($action->getId());
            if ( !in_array($title, $titles) ){
                $titles[$action->getId()] = $customAcl::titleFor($action->getId());
            }
        }
        $titles['Otros']='Otros';
        return $titles;
    }
    
    public function getActions(){
        if(!isset($this->ACL['class'])){
            throw new \Exception('Debes especificar la clas contenedora de los permisos');
        }
        $repoAction = $this->em->getRepository($this->classes['action']['class']);
        $acciones = $repoAction->findAll();
        $this->setTitleForActions($acciones);
        return $acciones;
    }
    
    private function setTitleForActions(&$actions){
        $customAcl = new $this->ACL['class'];
        foreach ($actions as $accion) {
            $accion->setTitle($customAcl::titleFor($accion->getId()));
        }
    }
    
    public function getAction($idAction){
        $repoAction = $this->em->getRepository($this->classes['action']['class']);
        return $repoAction->find($idAction);
    }
    public function getPermissionsUser($idUser){
        $repoUser = $this->em->getRepository($this->classes['user']['class']);
        $askedUser = $repoUser->find($idUser);
        $profile = $askedUser->getProfile();
        return $profile->getActions();
    }
    public function getPermissionsProfile($idProfile){
        $repoProfile = $this->em->getRepository($this->classes['profile']['class']);
        $profile = $repoProfile->find($idProfile);
        return $profile->getActions();
    }
    public function setPermissions($idProfile, $idsActions){
        $repoProfile = $this->em->getRepository($this->classes['profile']['class']);
        $profile = $repoProfile->find($idProfile);
        $repoAction = $this->em->getRepository($this->classes['action']['class']);
        //first we delete unwanted permissions 
        $previouslyAllowedActions = array();
        foreach($profile->getActions() as $previousAction){
            $previouslyAllowedActions[] = $previousAction->getId();
        }
        $actionsToDelete = array_diff($previouslyAllowedActions, $idsActions);
        foreach($actionsToDelete as $actionToDelete){
            $profile->removeAction($repoAction->find($actionToDelete));
        }
        //we add new permissions
        $actionsToInclude = array_diff($idsActions,$previouslyAllowedActions);
        
            
        foreach($actionsToInclude as $actionToInclude){
            $profile->addAction($repoAction->find($actionToInclude));
        }
        $this->em->flush();
        return $profile->getActions();
    }
    /**
     * @param $this->class["profile"]["class"] $profile
     * @return type
     */
    public function getPermissionsForm($profile, $actions = null){
        if ( !($profile instanceof $this->classes["profile"]["class"]) ){
            $allowed_actions = $this->getPermissionsProfile($profile);
        } else {
            $allowed_actions = $profile->getActions();
        }
        if ( is_null($actions) ){
            $actions = $this->getActions();
        }
        $this->setTitleForActions($actions);
        
        $defaults = array();
        foreach ($allowed_actions as $action) {
            $defaults[$action->getId()] = true;
        }
        $form_acciones_builder = $this->container->get('form.factory')->createBuilder(FormType::class, $defaults);
        
        foreach ($actions as $accion) {
            $form_acciones_builder->add($accion->getId(), CheckboxType::class, array(
                'label'=>$accion->getDisplay(),
                'required'=>false,
                'attr'=>array('title'=>$accion->getTitle())
            ));
        }
        
        $form_acciones_builder->add('guardar', SubmitType::class);

        return $form_acciones_builder->getForm();
    }
    public function sendUserActivationEmail($idUser){
        $repoUser = $this->em->getRepository($this->classes['user']['class']);
        $user = $repoUser->find($idUser);
        if($user->getState()==BaseUser::ESTADO_ACTIVO) {return false;}
        if($user->getState()==BaseUser::ESTADO_BLOQUEADO) {return false;}
        //mandar el mail. 
    }
    
    public function setPermissionsFromForm(Form $form, $profile){
        if ( !($profile instanceof $this->classes["profile"]["class"]) ){
            throw new Exception("Perfil no pertenece a la clase definida");
        }
        $new_actions = array();
        foreach ($form->getData() as $action_id => $assigned) {
            if($assigned){
                $new_actions[] = $action_id;
            }
        }
        $this->setPermissions($profile->getId(), $new_actions);
    }
}
