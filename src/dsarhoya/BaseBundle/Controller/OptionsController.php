<?php

namespace dsarhoya\BaseBundle\Controller;

use dsarhoya\BaseBundle\Entity\Option;
use Symfony\Component\HttpFoundation\Request;

class OptionsController extends BaseController
{
    public function indexAction()
    {
        return $this->render('@dsarhoyaBase/Options/index.html.twig');
    }

    public function maintenanceAction(Request $request)
    {
        $in_maintenance = $this->get('dsarhoya.base.options')->getMaintenanceState() ? true : false;

        $form = $this->createFormBuilder($this->get('dsarhoya.base.options')->getMaintenanceState() ? $this->get('dsarhoya.base.options')->getMaintenanceState() : null)
                ->add(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE, null, [
                    'label' => 'Título',
                    'required' => true,
                    'disabled' => $in_maintenance,
                ])
                ->add(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE, null, [
                    'label' => 'Mensaje',
                    'required' => true,
                    'disabled' => $in_maintenance,
                ])
                ->add('create', 'submit', [
                    'label' => 'Crear mantenimiento',
                    'attr' => [
                        'class' => 'btn btn-primary',
                    ],
                    'disabled' => $in_maintenance,
                ])
                ->add('remove', 'submit', [
                    'label' => 'Eliminar mantenimiento',
                    'attr' => [
                        'class' => 'btn btn-success',
                    ],
                    'disabled' => !$in_maintenance,
                ])
                ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($form->get('create')->isClicked()) {
                    if (!$this->get('dsarhoya.base.options')->setMaintenanceState(
                            $form->get(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE)->getData(),
                            $form->get(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE)->getData()
                            )) {
                        $this->flash($this->get('dsarhoya.base.options')->getErrorsAsString(), BaseController::FLASH_TYPE_ERROR);
                    }

                    return $this->redirect($this->generateUrl('adminOptionsMaintenance'));
                } elseif ($form->get('remove')->isClicked()) {
                    if (!$this->get('dsarhoya.base.options')->removeMaintenanceState()) {
                        $this->flash($this->get('dsarhoya.base.options')->getErrorsAsString(), BaseController::FLASH_TYPE_ERROR);
                    }

                    return $this->redirect($this->generateUrl('adminOptionsMaintenance'));
                }
            }
        }

        return $this->render('@dsarhoyaBase/Options/maintenance.html.twig', [
            'form' => $form->createView(),
            'in_maintenance' => $in_maintenance,
        ]);
    }
}
