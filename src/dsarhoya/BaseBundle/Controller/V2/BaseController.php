<?php

namespace dsarhoya\BaseBundle\Controller\V2;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    
    CONST FLASH_TYPE_INFO = 'info';
    CONST FLASH_TYPE_SUCCESS = 'success';
    CONST FLASH_TYPE_ERROR = 'error';
    CONST FLASH_TYPE_WARNING = 'warning';
    
    protected function flash($message, $type = self::FLASH_TYPE_INFO){
        $this->get('session')->getFlashBag()->add(
            $type,
            $message
        );
    }

    protected function flashInfo($message){
        $this->flash($message, self::FLASH_TYPE_INFO);
    }

    protected function flashSuccess($message){
        $this->flash($message, self::FLASH_TYPE_SUCCESS);
    }

    protected function flashError($message){
        $this->flash($message, self::FLASH_TYPE_ERROR);
    }

    protected function flashWarning($message){
        $this->flash($message, self::FLASH_TYPE_WARNING);
    }
}
