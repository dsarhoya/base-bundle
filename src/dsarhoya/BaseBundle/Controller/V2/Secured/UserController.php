<?php

namespace dsarhoya\BaseBundle\Controller\V2\Secured;

use dsarhoya\BaseBundle\Event\BaseBundleEvent;
use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use dsarhoya\BaseBundle\Services\OptionsService;
use dsarhoya\BaseBundle\Services\ParametersService;
use dsarhoya\BaseBundle\Services\UserKeysService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user-identification", name="base_bundle_user_identification")
     */
    public function userIdentification(Request $request, ParametersService $parametersService, OptionsService $optionsService, AuthorizationCheckerInterface $authorizationChecker, UserKeysService $userKeysService, EventDispatcherInterface $eventDispatcher)
    {
        $routes = $parametersService->routes;

        if($optionsService->getMaintenanceState() && !$authorizationChecker->isGranted('ROLE_SUPER_ADMIN')){
            return $this->redirect($this->generateUrl('base_bundle_logout'));
        }
        
        foreach ($routes as $role => $role_options) {
            if ($authorizationChecker->isGranted($role)) {
        
                //call the services tagged with 'login'
                // $executables = $this->get('dsarhoya.base.execute');
                // $executables->execute(BaseExecute::LOGIN);
                $eventDispatcher->dispatch(new BaseBundleEvent(), BaseBundleEvents::LOGIN);
                // dd('evento');
                //we dont create a new user key for super admin and free riders
                if(!$authorizationChecker->isGranted('ROLE_SUPER_ADMIN')){
                    if($this->getUser()->isFreeRider()!=1){
                        if(is_subclass_of($this->getUser(), 'dsarhoya\BaseBundle\Entity\BaseUser') && $parametersService->uniqueUser){

                            $key = $userKeysService->newConcurrentUserKey($this->getUser());

                            $session = $request->getSession();
                            $session->set('userKey', $key->getPrivateKey());
                        }
                    }
                }
                
                return $this->redirect($this->generateUrl($role_options['route']));
            }
        }
        
        throw new \Exception('No se encontró ruta inicial para este rol');

    }
}
