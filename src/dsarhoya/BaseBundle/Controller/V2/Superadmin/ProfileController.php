<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use dsarhoya\BaseBundle\Form\Type\ProfileType;
use dsarhoya\BaseBundle\Services\ParametersService;
use dsarhoya\BaseBundle\Services\PermissionsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{companyId}/profiles")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="base_bundle_superadmin_profiles_index")
     * @Route("/", name="adminGetProfiles")
     */
    public function index($companyId, ParametersService $parametersService)
    {
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);

        return $this->render("@dsarhoyaBase/Profile/getAll.html.twig", [
            'company' => $company
        ]);
    }

    /**
     * @Route("/{profileId}/permissions", name="base_bundle_superadmin_profiles_permissions")
     * @Route("/{profileId}/permissions", name="adminUpdateProfilePermissions")
     */
    public function permissions(Request $request, $companyId, $profileId, ParametersService $parametersService, PermissionsService $permissionsService)
    {
        $repoProfile = $this->getDoctrine()->getRepository($parametersService->classes['profile']['class']);
        $profile = $repoProfile->find($profileId);
        
        // $auth = $this->get('dsarhoya.authentication');
        $form = $permissionsService->getPermissionsForm($profile);
        $titles = $permissionsService->getActionTitles();
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $nuevos_permisos = array();
            foreach ($form->getData() as $id_permiso => $asignado) {
                if($asignado){
                    $nuevos_permisos[] = $id_permiso;
                }
            }

            $permissionsService->setPermissions($profileId, $nuevos_permisos);
            return $this->redirect($this->generateUrl('adminUpdateProfilePermissions', [
                'profileId' => $profileId,
                'companyId' => $companyId,
            ]));
        }
        
        return $this->render('@dsarhoyaBase/Permission/edit.html.twig',
                array(
                    'form'=>$form->createView(),
                    'titles'=>$titles,
                    'profile'=>$profile
                ));

    }

    /**
     * @Route("/{profileId}/edit", name="base_bundle_superadmin_profiles_edit")
     * @Route("/{profileId}/edit", name="adminUpdateProfile")
     */
    public function edit(Request $request, $companyId, $profileId, ParametersService $parametersService)
    {

        $repoProfile = $this->getDoctrine()->getRepository($parametersService->classes['profile']['class']);
        $profile = $repoProfile->find($profileId);
        
        $form = $this->createForm(ProfileType::class, $profile, array(
            'admin'=>true
        ));

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('adminGetProfiles', array('companyId'=>$companyId)));
        }
        
        return $this->render('@dsarhoyaBase/Profile/updateProfile.html.twig',
                array('form'=>$form->createView()));
    }

    /**
     * @Route("/new", name="base_bundle_superadmin_profiles_new")
     * @Route("/new", name="adminPutProfile")
     */
    public function new(Request $request, $companyId, ParametersService $parametersService)
    {
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        $profile = new $parametersService->classes['profile']['class'];
        
        $company->addProfile($profile);
        $profile->setCompany($company);
        
        $form = $this->createForm(ProfileType::class, $profile, array(
            'admin' => true,
            'attr' => ['id'=>'formCrearPerfil'],
            'action'=>$this->generateUrl('adminPutProfile', ['companyId'=> $company->getId()])
        ));

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();
            return $this->redirect($this->generateUrl('adminGetProfiles', array('companyId' => $companyId)));
        }
        return $this->render('@dsarhoyaBase/Profile/putProfile.html.twig',
                array('form'=>$form->createView(), 'company'=>$company));
    }

    /**
     * @Route("/{profileId}/delete", name="base_bundle_superadmin_profiles_delete")
     * @Route("/{profileId}/delete", name="adminDeleteProfile")
     */
    public function delete(Request $request, $companyId, $profileId, ParametersService $parametersService)
    {
        $repoProfile = $this->getDoctrine()->getRepository($parametersService->classes['profile']['class']);
        $profile = $repoProfile->find($profileId);
        
        $users = $profile->getUsers();
        foreach ($users as $user) {
            $user->setProfile(null);
        }
        
        $this->getDoctrine()->getManager()->remove($profile);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirect($this->generateUrl('adminGetProfiles', array('companyId'=>$companyId)));
    }
}
