<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use dsarhoya\BaseBundle\Controller\V2\BaseController;
use dsarhoya\BaseBundle\Entity\Option;
use dsarhoya\BaseBundle\Form\Type\Options\MaintenanceType;
use dsarhoya\BaseBundle\Services\OptionsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/options")
 */
class OptionsController extends BaseController
{
    /**
     * @Route("/", name="base_bundle_superadmin_options_index")
     */
    public function index()
    {
        return $this->render('@dsarhoyaBase/Options/index.html.twig');
    }

    /**
     * @Route("/maintenance", name="base_bundle_superadmin_options_maintenance")
     */
    public function maintenance(Request $request, OptionsService $optionsService)
    {
        $in_maintenance = $optionsService->getMaintenanceState() ? true : false;
        
        // tiene un bug raro. Hay que pasar esto a un form
        $form = $this->createForm(MaintenanceType::class, null, [
            'in_maintenance' => $in_maintenance,
        ]);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            if ($form->get('create')->isClicked()) {
                if (!$optionsService->setMaintenanceState(
                        $form->get(Option::OPTION_ID_UNDER_MAINTENANCE_TITLE)->getData(),
                        $form->get(Option::OPTION_ID_UNDER_MAINTENANCE_MESSAGE)->getData()
                    )) {
                    $this->flashError($optionsService->getErrorsAsString());
                }

                return $this->redirect($this->generateUrl('base_bundle_superadmin_options_index'));
            } elseif ($form->get('remove')->isClicked()) {
                if (!$optionsService->removeMaintenanceState()) {
                    $this->flashError($optionsService->getErrorsAsString());
                }

                return $this->redirect($this->generateUrl('base_bundle_superadmin_options_index'));
            }
            
        }

        return $this->render('@dsarhoyaBase/Options/maintenance.html.twig', [
            'form' => $form->createView(),
            'in_maintenance' => $in_maintenance,
        ]);
    }
}
