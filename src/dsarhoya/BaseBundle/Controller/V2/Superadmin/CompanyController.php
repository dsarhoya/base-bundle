<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use dsarhoya\BaseBundle\Services\CompanyManagementService;
use dsarhoya\BaseBundle\Services\ParametersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/companies")
 */
class CompanyController extends AbstractController
{
    /**
     * @Route("/", name="base_bundle_superadmin_company_index")
     * @Route("/", name="adminGetCompany")
     */
    public function index(ParametersService $parametersService)
    {
        /** @todo soft delete! */
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $companies = $repoCompany->findAll();
        
        return $this->render("@dsarhoyaBase/Company/getAll.html.twig", [
            'companies' => $companies
        ]);
    }


    /**
     * @Route("/{companyId}/get", name="base_bundle_superadmin_company_get")
     */
    public function getCompany($companyId, ParametersService $parametersService)
    {
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        return $this->render("@dsarhoyaBase/Company/get.html.twig", [
            'company' => $company
        ]);
    }


    /**
     * @Route("/new", name="base_bundle_superadmin_company_new")
     * @Route("/new", name="adminPutCompany")
     */
    public function new(Request $request, ParametersService $parametersService, CompanyManagementService $companyManagementService)
    {

        $company = new $parametersService->classes['company']['class'];
        $form = $companyManagementService->getForm($company,array("all"),false, null);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $companyManagementService->persistCompany($company);
            return $this->redirect($this->generateUrl('adminGetCompany'));
        }

        return $this->render('@dsarhoyaBase/Company/putCompany.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/{companyId}/delete", name="base_bundle_superadmin_company_delete")
     * @Route("/{companyId}/delete", name="adminDeleteCompany")
     */
    public function delete($companyId, ParametersService $parametersService, CompanyManagementService $companyManagementService)
    {
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        $companyManagementService->removeCompany($company);
        return $this->redirect($this->generateUrl('adminGetCompany'));
    }


    /**
     * @Route("/{companyId}/edit", name="base_bundle_superadmin_company_edit")
     * @Route("/{companyId}/edit", name="adminUpdateCompany")
     */
    public function edit(Request $request, $companyId, ParametersService $parametersService, CompanyManagementService $companyManagementService)
    {
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        
        $form = $companyManagementService->getForm($company,array("all"),false, null);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $companyManagementService->updateCompany($company);
            return $this->redirect($this->generateUrl('adminGetCompany'));
        }

        return $this->render('@dsarhoyaBase/Company/updateCompany.html.twig',[
            'form'=>$form->createView(),
            'company'=>$company
        ]);
    }


    /**
     * @Route("/{companyId}/toggle-state", name="base_bundle_superadmin_company_toggle_state")
     */
    public function toggleState($companyId, ParametersService $parametersService)
    {
        
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        
        if($company->getState()==1){
            $company->setState(0);
            $auxReturn = '<i class="fa fa-ban"></i>';
        }else{
            $company->setState(1);
            $auxReturn = '<i class="fa fa-check"></i>';
        }

        $this->getDoctrine()->getManager()->flush();
        return new Response($auxReturn);
    }
}
