<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use dsarhoya\BaseBundle\Services\BaseKeysService;
use dsarhoya\BaseBundle\Services\Options\BaseKeys\SendAccountValidationEmailOptions;
use dsarhoya\BaseBundle\Services\ParametersService;
use dsarhoya\BaseBundle\Services\UserKeysService;
use dsarhoya\BaseBundle\Services\UserManagementService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{companyId}/users")
 */
class UserController extends AbstractController
{
    
    /**
     * @Route("/", name="base_bundle_superadmin_users_index")
     * @Route("/", name="adminGetUsers")
     */
    public function index($companyId, ParametersService $parametersService)
    {
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        
        return $this->render("@dsarhoyaBase/User/getAll.html.twig", 
                array('users'=>$company->getUsers(), 'company'=>$company));
    }

    /**
     * @Route("/{userId}/toggle-state", name="base_bundle_superadmin_users_toggle_state")
     * @Route("/{userId}/toggle-state", name="adminToggleUserState")
     */
    public function toggleState($companyId, $userId, ParametersService $parametersService, UserManagementService $userManagementService){
        $repo = $this->getDoctrine()->getRepository($parametersService->classes['user']['class']);
        $user = $repo->find($userId);

        if($userManagementService->toggleUserState($user)==0){
            $auxReturn = '<i class="fa fa-ban"></i>';
        }else{
            $auxReturn = '<i class="fa fa-check"></i>';
        }
        $response = new Response($auxReturn);
        return $response;
    }

    /**
     * @Route("/{userId}/send-validation-email", name="base_bundle_superadmin_users_send_validation_email")
     * @Route("/{userId}/send-validation-email", name="adminSendValidationEmail")
     */
    public function sendValidationEmail($companyId, $userId, ParametersService $parametersService, BaseKeysService $baseKeysService)
    {
        
        $repo = $this->getDoctrine()->getRepository($parametersService->classes['user']['class']);
        $userToValidate = $repo->find($userId);

        if($baseKeysService->sendAccountValidationEmail(new SendAccountValidationEmailOptions(['user' => $userToValidate]))){
            $response = new Response('Correo de activación enviado.');
            return $response;
        }else{
            $response = new Response($baseKeysService->getErrorsAsString());
            $response->setStatusCode(500);
            return $response;
        }
    }

    /**
     * @Route("/{userId}/edit", name="base_bundle_superadmin_users_edit")
     */
    public function edit(Request $request, $companyId, $userId, ParametersService $parametersService, UserManagementService $userManagementService){
        $repoUser = $this->getDoctrine()->getRepository($parametersService->classes['user']['class']);
        $user = $repoUser->find($userId);
        
        $form = $userManagementService->getForm($user,array("all"),true);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $userManagementService->updateUser($user);
            return $this->redirect($this->generateUrl('base_bundle_superadmin_users_index', array('companyId'=>$companyId)));
        }

        return $this->render('@dsarhoyaBase/User/updateUser.html.twig',
                array('form'=>$form->createView(),'user'=>$user));
    }

    /**
     * @Route("/{userId}/change-password", name="base_bundle_superadmin_users_change_password")
     */
    public function changeUserPassword(Request $request, $companyId, $userId, ParametersService $parametersService, UserManagementService $userManagementService){
        
        $repoUser = $this->getDoctrine()->getRepository($parametersService->classes['user']['class']);
        $user = $repoUser->find($userId);
        
        $form = $userManagementService->getUpdatePasswordForm( $this->generateUrl('base_bundle_superadmin_users_change_password', [
            'userId' => $user->getId(),
            'companyId' => $companyId,
        ]));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            if($userManagementService->updatePassword($user,$form)){
                return $this->redirect($this->generateUrl('base_bundle_superadmin_users_index', array('companyId'=>$companyId)));
            }
        }
        return $this->render('@dsarhoyaBase/User/changePassword.html.twig',
                array('form'=>$form->createView(), 'user'=>$user));
    }

    /**
     * @Route("/{userId}/delete", name="base_bundle_superadmin_users_delete")
     */
    public function delete($companyId, $userId, ParametersService $parametersService){
        $repoUser = $this->getDoctrine()->getRepository($parametersService->classes['user']['class']);
        $user = $repoUser->find($userId);
        
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirect($this->generateUrl('base_bundle_superadmin_users_index', array('companyId'=>$companyId)));
    }
    
    /**
     * @Route("/new", name="base_bundle_superadmin_users_new")
     */
    public function new(Request $request, $companyId, ParametersService $parametersService, UserManagementService $userManagementService){
        $repoCompany = $this->getDoctrine()->getRepository($parametersService->classes['company']['class']);
        $company = $repoCompany->find($companyId);
        
        $user = new $parametersService->classes['user']['class'];
        $user->setCompany($company);
        
        
        $form = $userManagementService->getForm($user,array("all"),true);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $userManagementService->persistUser($user);
            return $this->redirect($this->generateUrl('base_bundle_superadmin_users_index', array('companyId'=>$companyId)));
        }
        return $this->render('@dsarhoyaBase/User/putUser.html.twig',
                array('form'=>$form->createView(), 'company'=>$company));
    }
}
