<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use dsarhoya\BaseBundle\Services\ParametersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/extensions")
 */
class ExtensionsController extends AbstractController
{
    /**
     * @Route("/", name="base_bundle_superadmin_extensions_index")
     */
    public function index(ParametersService $parametersService)
    {
        $extensions = $parametersService->adminMenuExtension;
        
        return $this->render('@dsarhoyaBase/Admin/adminMenuExtensions.html.twig', [
            'extensions'=>$extensions
        ]);
    }
}
