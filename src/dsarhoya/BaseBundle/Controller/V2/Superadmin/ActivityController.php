<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/activity")
 */
class ActivityController extends AbstractController
{
    /**
     * @Route("/", name="base_bundle_superadmin_activity_index")
     * @Route("/", name="adminActivityIndex")
     */
    public function index()
    {
        throw new \Exception('falta');
    }
}
