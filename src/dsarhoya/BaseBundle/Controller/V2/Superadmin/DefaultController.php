<?php

namespace dsarhoya\BaseBundle\Controller\V2\Superadmin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/index", name="base_bundle_superadmin_default_index")
     * @Route("/index", name="indexAdmin")
     */
    public function index()
    {
        return $this->render('@dsarhoyaBase/Admin/index.html.twig');
    }
}
