<?php

namespace dsarhoya\BaseBundle\Controller\V2\Pub;

use dsarhoya\BaseBundle\Services\OptionsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="base_bundle_login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils, OptionsService $optionsService)
    {
        if ($optionsService->getMaintenanceState() && !$request->query->get('admin')) {
            return $this->render('@dsarhoyaBase/Login/maintenance.html.twig', $optionsService->getMaintenanceState());
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@dsarhoyaBase/Login/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="base_bundle_logout")
     */
    public function logout()
    {
        throw new \Exception('Will be intercepted before getting here');
    }
}
