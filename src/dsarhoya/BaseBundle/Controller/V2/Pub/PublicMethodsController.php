<?php

namespace dsarhoya\BaseBundle\Controller\V2\Pub;

use dsarhoya\BaseBundle\Entity\UserKey;
use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use dsarhoya\BaseBundle\Security\Provider\BaseUserProviderInterface;
use dsarhoya\BaseBundle\Services\ParametersService;
use dsarhoya\BaseBundle\Services\UserKeysService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class PublicMethodsController extends AbstractController
{
    /**
     * @Route("/ask-retrieve-password", name="base_bundle_ask_retrieve_password")
     */
    public function askRetrievePassword(Request $request, UserKeysService $userKeysService, BaseUserProviderInterface $baseUserProvider)
    {
        $form = $this->createFormBuilder()
                    ->add('email', null, [
                        'required' => false,
                        'label' => 'Email',
                        ])
                    ->getForm();

        $message = null;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $baseUserProvider->loadUserByUsername($form->get('email')->getData());

            if (!$user) {
                $message = 'Ha ocurrido un error, por favor intente de nuevo';
            } else {
                if ($userKeysService->sendResetPasswordKey($user)) {
                    $message = 'Te hemos enviado un correo, úsalo para recuperar tu contraseña';
                } else {
                    $message = 'Ha ocurrido un error, por favor intente de nuevo';
                }
            }
        }

        return $this->render('@dsarhoyaBase/User/askRetrievePassword.html.twig',
            ['message' => $message, 'form' => $form->createView()]
        );
    }

    /**
     * @Route("/password/retrieve/{key}", name="retrievePassword", methods={"GET","POST"})
     */
    public function retrievePasswordAction(Request $request, $key, UserKeysService $userKeysService)
    {
        /** @var Userkey $userKey */
        $userKey = $userKeysService->validateKey($key, UserKey::END_RETRIEVE_PASSWORD);
        if ($userKey) {
            $form = $userKeysService->getChangePasswordForm($userKey, UserKey::END_RETRIEVE_PASSWORD);

            $message = null;
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                if ($userKeysService->changePassword($key, $data)) {
                    $event = $this->dispatch(BaseBundleEvents::POST_RETRIEVE_PASSWORD, ['entity' => $userKey->getUser()]);

                    if ($event->hasResponse()) {
                        return $event->getResponse();
                    }
                    $this->addFlash('success', 'Cambio de contraseña éxitoso. Ahora inicia sesión con tus nuevas credenciales.');

                    return $this->redirect($this->generateUrl('base_bundle_login'));
                } else {
                    $message = $userKeysService->getErrorsAsString();
                }
            }

            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', ['form' => $form->createView(), 'message' => $message]);
        } else {
            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', ['message' => 'No se puede recuperar la contraseña']);
        }
    }

    /**
     * @Route("/logout", name="base_bundle_logout")
     */
    public function logout()
    {
        throw new \Exception('Will be intercepted before getting here');
    }

    /**
     * @Route("/validate-accotun", name="base_bundle_validate_account")
     * @Route("/validate-accotun/{key}", name="validateAccount")
     */
    public function validateAccountAction(Request $request, $key, UserKeysService $userKeysService, ParametersService $parametersService)
    {
        $user_key = $userKeysService->validateKey($key, UserKey::END_VALIDATE_USER);

        /* @var $user_key UserKey */
        if ($user_key) {
            $message = null;
            $form = $userKeysService->getChangePasswordForm($user_key, UserKey::END_VALIDATE_USER);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                $classes = $parametersService->classes;
                if ($userKeysService->validateUser($key, $data, $classes['user']['class'])) {
                    $event = $this->dispatch(BaseBundleEvents::POST_VALIDATE_ACCOUNT, ['entity' => $user_key->getUser()]);

                    if ($event->hasResponse()) {
                        return $event->getResponse();
                    }

                    /* @todo esto debiera ser configurado */
                    return $this->redirect($this->generateUrl('base_bundle_login'));
                } else {
                    $message = $userKeysService->getErrorsAsString();
                }
            }

            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', ['title' => 'Para activar tu cuenta ingresa una contraseña', 'form' => $form->createView(), 'message' => $message]);
        } else {
            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', ['title' => 'Para activar tu cuenta ingresa una contraseña', 'message' => 'No se puede recuperar la contraseña']);
        }
    }

    /**
     * @param string $eventName
     */
    protected function dispatch($eventName, array $arguments = [])
    {
        $subject = $arguments['entity'];
        $event = new GetResponseForControllerResultEvent($this->container->get('kernel'), $this->get('request_stack')->getCurrentRequest(), HttpKernelInterface::MASTER_REQUEST, $subject);
        $this->get('event_dispatcher')->dispatch($eventName, $event);

        return $event;
    }
}
