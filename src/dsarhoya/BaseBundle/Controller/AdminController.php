<?php

namespace dsarhoya\BaseBundle\Controller;

use dsarhoya\BaseBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use dsarhoya\BaseBundle\Form\Type\ProfileType;
use dsarhoya\BaseBundle\Services\Options\BaseKeys\SendAccountValidationEmailOptions;

use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminController extends BaseController
{
    private $classes;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->classes = $this->container->getParameter('dsarhoya_base.classes');
    }
    
    public function indexAction()
    {
        return $this->render('@dsarhoyaBase/Admin/index.html.twig');
    }
    /*
     * COMPANY
     */
    public function getCompanyAction($idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        if(is_null($idCompany)){
            $companies = $repoCompany->findAll();
            return $this->render("@dsarhoyaBase/Company/getAll.html.twig", 
                    array('companies'=>$companies));
        }else{
            $company = $repoCompany->find($idCompany);
            return $this->render("dsarhoyaBaseBundle:Company:getOne.html.twig", 
                    array('company'=>$company));
        }
    }
    public function putCompanyAction(Request $request){
        $company = new $this->classes['company']['class'];
        $companyManagement = $this->get("dsarhoya.base.companymanagement");
        $form = $companyManagement->getForm($company,array("all"),false, null);
        if($request->getMethod()=='POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $companyManagement->persistCompany($company);
                return $this->redirect($this->generateUrl('adminGetCompany'));
            }
        }
        return $this->render('@dsarhoyaBase/Company/putCompany.html.twig',
                array('form'=>$form->createView()));
    }
    public function updateCompanyAction(Request $request, $idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        $companyManagement = $this->get("dsarhoya.base.companymanagement");
        $form = $companyManagement->getForm($company,array("all"),false, null);
        if($request->getMethod()=='POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $companyManagement->updateCompany($company);
                return $this->redirect($this->generateUrl('adminGetCompany'));
            }
        }
        return $this->render('@dsarhoyaBase/Company/updateCompany.html.twig',
                array(
                    'form'=>$form->createView(),
                    'company'=>$company
                ));
    }
    public function deleteCompanyAction($idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        $companyManagement = $this->get("dsarhoya.base.companymanagement");
        $companyManagement->removeCompany($company);
        return $this->redirect($this->generateUrl('adminGetCompany'));
    }
    public function toggleCompanyStateAction($idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        if($company->getState()==1){
            $company->setState(0);
            $auxReturn = '<i class="fa fa-ban"></i>';
        }else{
            $company->setState(1);
            $auxReturn = '<i class="fa fa-check"></i>';
        }
        $this->getDoctrine()->getManager()->flush();
        $response = new Response($auxReturn);
        return $response;
    }
    /*
     * USER
     */
    public function getUsersAction($idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        
        return $this->render("@dsarhoyaBase/User/getAll.html.twig", 
                array('users'=>$company->getUsers(), 'company'=>$company));
        
    }
    public function putUserAction(Request $request, $idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        
        $user = new $this->classes['user']['class'];
        $user->setCompany($company);
        $userManagement = $this->get("dsarhoya.base.usermanagement");
        $form = $userManagement->getForm($user,array("all"),true);
        if($request->getMethod()=='POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $userManagement->persistUser($user);
                return $this->redirect($this->generateUrl('adminGetUsers', array('idCompany'=>$company->getId())));
            }
        }
        return $this->render('@dsarhoyaBase/User/putUser.html.twig',
                array('form'=>$form->createView(), 'company'=>$company));
    }
    public function deleteUserAction($idUser){
        $repoUser = $this->getDoctrine()->getRepository($this->classes['user']['class']);
        $user = $repoUser->find($idUser);
        $idCompany = $user->getCompany()->getId();
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('adminGetUsers', array('idCompany'=>$idCompany)));
    }
    public function updateUserAction(Request $request,$idUser){
        $repoUser = $this->getDoctrine()->getRepository($this->classes['user']['class']);
        $user = $repoUser->find($idUser);
        $userManagement = $this->get("dsarhoya.base.usermanagement");
        $form = $userManagement->getForm($user,array("all"),true);
        if($request->getMethod()=='POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $userManagement->updateUser($user);
                return $this->redirect($this->generateUrl('adminGetUsers', array('idCompany'=>$user->getCompany()->getId())));
            }
        }
        return $this->render('@dsarhoyaBase/User/updateUser.html.twig',
                array('form'=>$form->createView(),'user'=>$user));
    }
    public function changeUserPasswordAction(Request $request, $idUser){
        
        $repoUser = $this->getDoctrine()->getRepository($this->classes['user']['class']);
        $user = $repoUser->find($idUser);
        $userManagement = $this->get("dsarhoya.base.usermanagement");
        $form = $userManagement->getUpdatePasswordForm( $this->generateUrl('adminChangeUserPassword', array('idUser'=>$user->getId()) ) );
        if($request->getMethod() == "POST"){
            $form->handleRequest($request);
            if($userManagement->updatePassword($user,$form)){
                return $this->redirect($this->generateUrl('adminGetUsers', array('idCompany'=>$user->getCompany()->getId())));
            }
        }
        return $this->render('@dsarhoyaBase/User/changePassword.html.twig',
                array('form'=>$form->createView(), 'user'=>$user));
    }
    public function toggleUserStateAction($idUser){
        $repo = $this->getDoctrine()->getRepository($this->classes['user']['class']);
        $user = $repo->find($idUser);
        $userManagement = $this->get("dsarhoya.base.usermanagement");
        if($userManagement->toggleUserState($user)==0){
            $auxReturn = '<i class="fa fa-ban"></i>';
        }else{
            $auxReturn = '<i class="fa fa-check"></i>';
        }
        $response = new Response($auxReturn);
        return $response;
    }
    public function sendValidationEmailAction($idUser){
        
        $repo = $this->getDoctrine()->getRepository($this->classes['user']['class']);
        $userToValidate = $repo->find($idUser);

        $keysHelper = $this->getServiceBaseKeys();
        
        if($keysHelper->sendAccountValidationEmail(new SendAccountValidationEmailOptions(['user' => $userToValidate]))){
            $response = new Response('Correo de activación enviado.');
            return $response;
        }else{
            $response = new Response($keysHelper->getErrorsAsString());
            $response->setStatusCode(500);
            return $response;
        }
        
    }   
    /*
     * PROFILE
     */
    public function getProfileAction($idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        return $this->render("@dsarhoyaBase/Profile/getAll.html.twig", 
                array('company'=>$company));
    }
    public function putProfileAction(Request $request, $idCompany){
        $repoCompany = $this->getDoctrine()->getRepository($this->classes['company']['class']);
        $company = $repoCompany->find($idCompany);
        $profile = new $this->classes['profile']['class'];
        $company->addProfile($profile);
        $profile->setCompany($company);
        
        $form = $this->createForm(ProfileType::class, $profile, array(
            'admin'=>true,
            'attr'=>['id'=>'formCrearPerfil'],
            'action'=>$this->generateUrl('adminPutProfile', ['idCompany'=> $company->getId()])
        ));
        if($request->getMethod()=='POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($profile);
                $em->flush();
                return $this->redirect($this->generateUrl('adminGetProfiles', array('idCompany'=>$idCompany)));
            }
        }
        return $this->render('@dsarhoyaBase/Profile/putProfile.html.twig',
                array('form'=>$form->createView(), 'company'=>$company));
    }
    public function deleteProfileAction($idProfile){
        $repoProfile = $this->getDoctrine()->getRepository($this->classes['profile']['class']);
        $profile = $repoProfile->find($idProfile);
        $users = $profile->getUsers();
        foreach ($users as $user) {
            $user->setProfile(null);
        }
        $idCompany = $profile->getCompany()->getId();
        $this->getDoctrine()->getManager()->remove($profile);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('adminGetProfiles', array('idCompany'=>$idCompany)));
    }
    
    public function updateProfileAction(Request $request,$idProfile){
        
        $repoProfile = $this->getDoctrine()->getRepository($this->classes['profile']['class']);
        $profile = $repoProfile->find($idProfile);
        
        $form = $this->createForm(ProfileType::class, $profile, array(
            'admin'=>true
        ));
        if($request->getMethod()=='POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->getDoctrine()->getManager()->flush();
                return $this->redirect($this->generateUrl('adminGetProfiles', array('idCompany'=>$profile->getCompany()->getId())));
            }
        }
        
        return $this->render('@dsarhoyaBase/Profile/updateProfile.html.twig',
                array('form'=>$form->createView()));
    }
    
    public function updateProfilePermissionsAction(Request $request,$idProfile){
        
        $repoProfile = $this->getDoctrine()->getRepository($this->classes['profile']['class']);
        $profile = $repoProfile->find($idProfile);
        
        $auth = $this->get('dsarhoya.authentication');
        $form = $auth->getPermissionsForm($idProfile);
        $titles = $auth->getActionTitles();
        
        if($request->getMethod() == "POST"){
            $form->handleRequest($request);
            $nuevos_permisos = array();
            foreach ($form->getData() as $id_permiso => $asignado) {
                if($asignado){
                    $nuevos_permisos[] = $id_permiso;
                }
            }

            $auth->setPermissions($idProfile, $nuevos_permisos);
            return $this->redirect($this->generateUrl('adminUpdateProfilePermissions', array('idProfile'=>$idProfile)));
        }
        
        return $this->render('@dsarhoyaBase/Permission/edit.html.twig',
                array(
                    'form'=>$form->createView(),
                    'titles'=>$titles,
                    'profile'=>$profile
                ));
    }
    
    public function adminMenuExtensionsAction(){
        
        $extensions = $this->container->getParameter('dsarhoya_base.admin_menu_extension');
        
        return $this->render('@dsarhoyaBase/Admin/adminMenuExtensions.html.twig',
                array(
                    'extensions'=>$extensions
                ));
    }
            
}
