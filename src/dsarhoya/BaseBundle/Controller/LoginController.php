<?php

namespace dsarhoya\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;    
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class LoginController extends Controller
{
    public function loginAction(Request $request)
    {
        if($this->get('dsarhoya.base.options')->getMaintenanceState() && !$request->query->get('admin')){
            return $this->render('@dsarhoyaBase/Login/maintenance.html.twig', $this->get('dsarhoya.base.options')->getMaintenanceState());
        }
        
        $session = $request->getSession();
        
        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
        }
        
        //we get the parameters and pass them to the template
        $templateParameters = array();
        $templateParameters['last_username'] = $session->get(Security::LAST_USERNAME);
        $templateParameters['error'] = $error;
        $aux = explode("?",$request->getRequestUri());
        if(count($aux)>1){
            $parameters = explode("&",$aux[1]);
            foreach ($parameters as $parameter){
                $auxParam = explode("=",$parameter);
                $templateParameters[$auxParam[0]]=$auxParam[1];
            }
        }
        
        return $this->render('@dsarhoyaBase/Login/login.html.twig',$templateParameters);
    }
    public function userIdentificationAction(){
        
        $routes = $this->container->getParameter('dsarhoya_base.routes');
        
        if($this->get('dsarhoya.base.options')->getMaintenanceState() && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            return $this->redirect($this->generateUrl('logout'));
        }
        
        foreach ($routes as $role => $role_options) {
            if ($this->get('security.authorization_checker')->isGranted($role)) {
        
                //call the services tagged with 'login'
                $executables = $this->get('dsarhoya.base.execute');
                $executables->execute(BaseExecute::LOGIN);
                
                //we dont create a new user key for super admin and free riders
                if(!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
                    if($this->getUser()->isFreeRider()!=1){
                        if(is_subclass_of($this->getUser(), 'dsarhoya\BaseBundle\Entity\BaseUser') 
                                && $this->container->getParameter('dsarhoya_base.uniqueUser')
                                ){

                            $key = $this->get('dsarhoya.base.userKeysService')->newConcurrentUserKey($this->getUser());

                            $session = $this->getRequest()->getSession();
                            $session->set('userKey', $key->getPrivateKey());
                        }
                    }
                }
                
                return $this->redirect($this->generateUrl($role_options['route']));
            }
        }
        
        throw new \Exception('No se encontró ruta inicial para este rol');
    }
}
