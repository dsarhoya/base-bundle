<?php

namespace dsarhoya\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use dsarhoya\BaseBundle\Entity\UserKey;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserController extends Controller
{
    private $classes;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->classes = $this->container->getParameter('dsarhoya_base.classes');
    }
    
    public function askRetrievePasswordAction(Request $request){
        
        $keysHelper = $this->get('dsarhoya.base.userKeysService');
        
        $form = $this->createFormBuilder()
                    ->add('email', null, array(
                        'required' => false,
                        'label'=>'Email',
                        ))
                    ->getForm();
        $message = null;
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            $repo = $this->getDoctrine()->getRepository($this->classes['user']['class']);
            $user = null;
            if ($repo instanceof UserLoaderInterface) {
                $user = $repo->loadUserByUsername($form->get('email')->getData());
            }else {
                $user = $repo->findOneBy(['email'=>$form->get('email')->getData()]);
            }
            if(!$user){
                $message = "Ha ocurrido un error, por favor intente de nuevo";
            }else{
                if($keysHelper->sendResetPasswordKey($user)){
                    $message = "Te hemos enviado un correo, úsalo para recuperar tu contraseña";
                }else{
                    $message = "Ha ocurrido un error, por favor intente de nuevo";
                }
                
            }
        }
        
        return $this->render('@dsarhoyaBase/User/askRetrievePassword.html.twig', 
                    array('message'=>$message, 'form'=>$form->createView())
                );
    }
    
    public function retrievePasswordAction(Request $request, $key){
        
        $keysHelper = $this->get('dsarhoya.base.userKeysService');
        
        $user_key = $keysHelper->validateKey($key, UserKey::END_RETRIEVE_PASSWORD);
        
        /* @var $user_key UserKey */
        if($user_key){
            
            $form = $keysHelper->getChangePasswordForm($user_key, UserKey::END_RETRIEVE_PASSWORD);

            $message = null;
            if($request->getMethod()=='POST'){

                $form->handleRequest($request);
                $data = $form->getData();
                
                if($keysHelper->changePassword($key, $data)){
                    $event = $this->dispatch(BaseBundleEvents::POST_RETRIEVE_PASSWORD, array('entity' => $user_key->getUser()));
                    
                    if ($event->hasResponse()) {
                        return $event->getResponse();
                    }
                    
                    return $this->redirect($this->generateUrl('login'));
                }else{
                    $message = $keysHelper->getErrorsAsString();
                }
            }
            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', array('form'=>$form->createView(), 'message'=>$message));
            
        }else{
            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', array('message'=>'No se puede recuperar la contraseña'));
        }   
    }
    
    public function validateAccountAction(Request $request, $key){
        
        $keysHelper = $this->get('dsarhoya.base.userKeysService');
        
        $user_key = $keysHelper->validateKey($key, UserKey::END_VALIDATE_USER);
        
        /* @var $user_key UserKey */
        if($user_key){
            
            $form = $keysHelper->getChangePasswordForm($user_key, UserKey::END_VALIDATE_USER);

            $message = null;
            if($request->getMethod()=='POST'){

                $form->handleRequest($request);
                $data = $form->getData();
                $classes = $this->container->getParameter('dsarhoya_base.classes');
                if($keysHelper->validateUser($key, $data, $classes['user']['class'])){
                    $event = $this->dispatch(BaseBundleEvents::POST_VALIDATE_ACCOUNT, array('entity' => $user_key->getUser()));
                    
                    if ($event->hasResponse()) {
                        return $event->getResponse();
                    }
                    
                    return $this->redirect($this->generateUrl('login'));
                }else{
                    $message = $keysHelper->getErrorsAsString();
                }
            }
            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', array('title'=>'Para activar tu cuenta ingresa una contraseña', 'form'=>$form->createView(), 'message'=>$message));
            
        }else{
            return $this->render('@dsarhoyaBase/User/retrievePassword.html.twig', array('title'=>'Para activar tu cuenta ingresa una contraseña', 'message'=>'No se puede recuperar la contraseña'));
        }   
    }
    
    public function sesionOverrideAction(){
        //revoke the user key
        $keyService = $this->get('dsarhoya.base.userKeysService');
        $userKey = $this->get('session')->get('userKey');
        $keyService->revokeUserKey($userKey);
        //throw an exception
        throw new AccessDeniedHttpException();
    }
    
    public function accessCodeAction(Request $request){
        $keyService = $this->get('dsarhoya.base.userKeysService');
        $session = $this->getRequest()->getSession();
        if($request->getMethod()=='POST'){
            $accessCode = $request->get('accessCode');
            if($keyService->checkAccessCode($accessCode,$session->get('userKey'))){
                $routes = $this->container->getParameter('dsarhoya_base.routes');
                foreach ($routes as $role => $role_options) {
                    if ($this->get('security.authorization_checker')->isGranted($role)) {
                        return $this->redirect($this->generateUrl($role_options['route']));
                    }
                }
            }else{
                return $this->redirect($this->generateUrl('logout'));
            }
        }else{
            //if we access here through a get call
            //we create a new user key
            $key = $keyService->newConcurrentUserKey($this->getUser());
            $session->set('userKey', $key->getPrivateKey());
            $keyService->sendAccessCodeEmail($key);
            return $this->render('@dsarhoyaBase/User/askAccessCode.html.twig');
        }
    }
    
    /**
     * @param string $eventName
     * @param array $arguments
     */
    protected function dispatch($eventName, array $arguments = array()) {
        $subject = $arguments['entity'];
        $event = new GetResponseForControllerResultEvent($this->container->get('kernel'), $this->get('request_stack')->getCurrentRequest(), HttpKernelInterface::MASTER_REQUEST, $subject);
        $this->get('event_dispatcher')->dispatch($eventName, $event);
        return $event;
    }
}
