<?php 

namespace AppBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use dsarhoya\BaseBundle\Event\ValidationUrlEvent;

/**
 * 
 */
class BaseBundleEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return array(
           BaseBundleEvents::GENERATE_VALIDATION_URL => 'generateValidationUrl'
        );
    }
    
    public function generateValidationUrl(ValidationUrlEvent $event){
        $event->setUrl('https://dsy.cl');
    }

}
