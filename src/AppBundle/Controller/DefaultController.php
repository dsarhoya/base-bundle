<?php

namespace AppBundle\Controller;

use dsarhoya\BaseBundle\Controller\BaseController;
use dsarhoya\BaseBundle\Services\Options\SendEmailOptions;
use dsarhoya\BaseBundle\Validator\Constraints\RUT;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="indexUser")
     */
    public function indexAction(Request $request)
    {
        $emailService = $this->getServiceEmails();
        $emailService->sendEmail(new SendEmailOptions([
            'to' => ['matias.castro+to@dsy.cl'],
            'cc' => ['matias.castro+cc@dsy.cl'],
            'bcc' => ['matias.castro+bcc@dsy.cl'],
            'subject' => 'test',
            'body' => 'test body',
        ]));

        dump($this->container->getParameter('dsarhoya_base.email_service'));
        exit;

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/file", name="user_file")
     * @Template()
     */
    public function fileAction(Request $request)
    {
        $form = $this->createFormBuilder()
               ->add('file', \Symfony\Component\Form\Extension\Core\Type\FileType::class, [
                   'label' => 'Archivo de usuarios',
               ])
                ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                    'label' => 'Subir',
                    'attr' => ['class' => 'btn btn-primary'],
                ])
                ->getForm();

        $form->handleRequest($request);

        dump($this->getServiceFiles()->fileUrl('test-files/algomas/file.png', ['signed' => true]));
        dump($this->getServiceFiles()->fileUrl('test-files/algomas/file.png', ['signed' => true]));

        if ($form->isSubmitted()) {
            $this->getServiceFiles()->AWSFileWithFileAndKey($form->get('file')->getData(), 'test-files/algomas/file.png');
            $uploader = new \dsarhoya\DSYFilesBundle\Listener\Uploader\LocalUploader();
            $uploader->upload($form->get('file')->getData(), 'test-files/algomas', 'file.png', []);
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/excel", name="user_excel")
     */
    public function excelAction(Request $request)
    {
        $form = $this->createFormBuilder()
               ->add('file', \Symfony\Component\Form\Extension\Core\Type\FileType::class, [
                   'label' => 'Archivo de usuarios',
               ])
                ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                    'label' => 'Subir',
                    'attr' => ['class' => 'btn btn-primary'],
                ])
                ->getForm();

        $loader = $this->get('dsarhoya.xls')->getExcelLoader();
        $loader->addCell(new \dsarhoya\DSYXLSBundle\XLS\Loader\ColumnOptions([
            'name' => 'email',
        ]));
        $loader->addCell(new \dsarhoya\DSYXLSBundle\XLS\Loader\ColumnOptions([
            'name' => 'pass',
        ]));
        $loader->addCell(new \dsarhoya\DSYXLSBundle\XLS\Loader\ColumnOptions([
            'name' => 'country',
        ]));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            dump($form->get('file')->getData());
            $res = $loader->load($form->get('file')->getData());

            foreach ($res as $fila) {
                dump($fila);
            }
            if (!$loader->isValid()) {
                dump($loader->getErrors());
            }
            exit;
        }

        return $this->render('AppBundle:Default:file.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/valida-rut", name="valida_rut")
     */
    public function validaRutAction()
    {
        $validator = $this->get('validator');
        $res = $validator->validate('1-9', new RUT());

        $rutTests = [];
        foreach ([
            '1-9',
            '1-8',
            '9814417-k',
            '9814417-K',
            'no-es-valido',
            '1.9',
            '1,9',
            '1guion9',
            '15147651-1',
            ] as $rut) {
            $rutTests[] = [
                'rut' => $rut,
                'violations' => (string) ($validator->validate($rut, new RUT())),
            ];
        }

        return $this->render('AppBundle:Default:rut_validation.html.twig', [
            'tests' => $rutTests,
        ]);
    }
}
