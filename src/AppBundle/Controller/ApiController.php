<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    
    /**
     * @Route("/test", name="api_index")
     */
    public function indexAction(Request $request)
    {
        echo 'api';
        die;
    }
}
