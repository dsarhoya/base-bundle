<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\BaseBundle\Entity\BaseAction;

/**
 * Action
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActionRepository")
 */
class Action extends BaseAction
{

    /**
     * @ORM\ManyToMany(targetEntity="Profile", mappedBy="actions")
     */
    private $profiles;

    /**
     * Add profile
     *
     * @param \AppBundle\Entity\Profile $profile
     *
     * @return Action
     */
    public function addProfile(\AppBundle\Entity\Profile $profile)
    {
        $this->profiles[] = $profile;

        return $this;
    }

    /**
     * Remove profile
     *
     * @param \AppBundle\Entity\Profile $profile
     */
    public function removeProfile(\AppBundle\Entity\Profile $profile)
    {
        $this->profiles->removeElement($profile);
    }

    /**
     * Get profiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfiles()
    {
        return $this->profiles;
    }
}
