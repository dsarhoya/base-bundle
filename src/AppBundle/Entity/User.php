<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\BaseBundle\Entity\BaseUser;
use dsarhoya\BaseBundle\Entity\BaseUserInterface;
// use dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\ApiKeyInterface;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser implements BaseUserInterface// , ApiKeyInterface
{
    /**
     * @ORM\OneToMany(targetEntity="dsarhoya\BaseBundle\Entity\UserKey", mappedBy="user")
     */
    private $keys;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Profile", inversedBy="users")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $profile;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $apiKey;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->keys = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add key
     *
     * @param \dsarhoya\BaseBundle\Entity\UserKey $key
     *
     * @return User
     */
    public function addKey(\dsarhoya\BaseBundle\Entity\UserKey $key)
    {
        $this->keys[] = $key;

        return $this;
    }

    /**
     * Remove key
     *
     * @param \dsarhoya\BaseBundle\Entity\UserKey $key
     */
    public function removeKey(\dsarhoya\BaseBundle\Entity\UserKey $key)
    {
        $this->keys->removeElement($key);
    }

    /**
     * Get keys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeys()
    {
        return $this->keys;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return User
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set profile
     *
     * @param \AppBundle\Entity\Profile $profile
     *
     * @return User
     */
    public function setProfile(\AppBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return User
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
    
    public function isActive() {
        return true;
    }
}
