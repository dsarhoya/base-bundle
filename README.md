Dsarhoya - Base bundle
===================

Toda la documentación está en la Wiki, en el siguiente link:

[Documentación](https://bitbucket.org/dsarhoya/base-bundle/wiki/Home)

Este es un proyecto de desarrollo, por lo que el archivo que tiene que usar composer es composer-dev.json:

```
COMPOSER=composer-dev.json composer update
```